DOCKER_BE=panel-backend-php

help: ## Muestra ayuda de los comandos
	@echo 'usage: make [target]'
	@echo 
	@echo 'target: '
	@egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s ':#'

run: ## Ejecuta los contenedores para la aplicacion
	docker network create panel-network || echo true
	docker compose up -d --remove-orphans

stop: ## Detiene los contenedores de la aplicacion
	docker compose stop

restart: ## Hace un restart de los contenedores
	$(MAKE) stop && $(MAKE) run

ssh-be: ## Se conecta con el contenedor de php
	docker exec -it ${DOCKER_BE} bash
