<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{

    protected $dontReport = [];
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Registra las excepciones que pueden ocurrir en el flujo de informacion
     */
    public function register(): void
    {
        $this->renderable(function (ItemNotFound $e) {
            return $this->getResponse($e->getMessage(), 404);
        });

        $this->renderable(function (UnauthorizedException $e) {
            return $this->getResponse($e->getMessage(), 401);
        });

        $this->renderable(function (PaymentRequiredException $e){
            return $this->getResponse($e->getMessage(), 401);
        });

        $this->renderable(function (BadCredentialsException $e) {
            return $this->getResponse($e->getMessage(), 401);
        });
    }

    private function getResponse($message, $code = 409)
    {
        return response()->json(
            [
                'status' => 'error',
                'message' => $message
            ], $code
        );
    }
}
