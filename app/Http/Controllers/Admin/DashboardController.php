<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Dashboard\BulkDestroyDashboard;
use App\Http\Requests\Admin\Dashboard\DestroyDashboard;
use App\Http\Requests\Admin\Dashboard\IndexDashboard;
use App\Http\Requests\Admin\Dashboard\StoreDashboard;
use App\Http\Requests\Admin\Dashboard\UpdateDashboard;
use App\Models\Dashboard;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Stripe\StripeClient;

class DashboardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexDashboard $request
     * @return array|Factory|View
     */
    public function index(IndexDashboard $request)
    {
        $stripe = new StripeClient(env('STRIPE_SECRET'));
        // Obtén todas las suscripciones
        $subscriptions = $stripe->subscriptions->all([
            'limit' => 100,
            'status' => 'all',
            'expand' => ['data.customer', 'data.plan.product']
        ]);

        $subscriptionsWithCustomerName = [];
        $statusCounts = [
            'incomplete' => 0,
            'incomplete_expired' => 0,
            'trialing' => 0,
            'active' => 0,
            'past_due' => 0,
            'canceled' => 0,
            'unpaid' => 0,
            'paused' => 0,
        ];

        foreach ($subscriptions->data as $subscription) {
            $customer = $subscription->customer;
            $plan = $subscription->plan;
            $product = $plan->product;

            // Agrega el nombre del cliente a la suscripción
            $subscriptionsWithCustomerName[] = [
                'id' => $subscription->id,
                'customer_name' => $customer->name,
                'customer_email' => $customer->email,
                'current_period_start' => $subscription->current_period_start,
                'current_period_end' => $subscription->current_period_end,
                'status' => $subscription->status,
                'subscription' => $subscription,
                'service_name' => $product->name,
                'currency' => $plan->currency,
                'object' => $subscription->object,
                'interval' => $plan->interval,
                'interval_count' => $plan->interval_count,
            ];

            if (array_key_exists($subscription->status, $statusCounts)) {
                $statusCounts[$subscription->status]++;
            }
        }

        $statusCountsList = [];
        foreach ($statusCounts as $status => $count) {
            $statusCountsList[] = [
                'name' => $status,
                'count' => $count,
            ];
        }

        foreach ($subscriptionsWithCustomerName as $subscriptionDetails) {
            $currentPeriodEnd = Carbon::createFromTimestamp($subscriptionDetails['current_period_end']);
            $currentPeriodStart = Carbon::createFromTimestamp($subscriptionDetails['current_period_start']);
            Dashboard::updateOrCreate(
                ['subscription_id' => $subscriptionDetails['id']],
                [
                    'subscription_id' => $subscriptionDetails['id'],
                    'nombre' => $subscriptionDetails['customer_name'],
                    'servicio' => $subscriptionDetails['service_name'],
                    'status' => $subscriptionDetails['status'],
                    'currency' => $subscriptionDetails['currency'],
                    'object' => $subscriptionDetails['object'],
                    'interval' => $subscriptionDetails['interval'],
                    'interval_count' => $subscriptionDetails['interval_count'],
                    'current_period_end' => $currentPeriodEnd,
                    'current_period_start' => $currentPeriodStart,
                ]
            );
        }
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Dashboard::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'subscription_id', 'nombre', 'servicio', 'status', 'currency', 'object', 'interval', 'interval_count', 'current_period_end', 'current_period_start'],

            // set columns to searchIn
            ['id', 'subscription_id', 'nombre', 'servicio', 'status', 'currency', 'object', 'interval', 'interval_count', 'current_period_end', 'current_period_start']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.dashboard.index', ['data' => $data], compact('statusCountsList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.dashboard.create');

        return view('admin.dashboard.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDashboard $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreDashboard $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Dashboard
        $dashboard = Dashboard::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/dashboards'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/dashboards');
    }

    /**
     * Display the specified resource.
     *
     * @param Dashboard $dashboard
     * @throws AuthorizationException
     * @return void
     */
    public function show(Dashboard $dashboard)
    {
        $this->authorize('admin.dashboard.show', $dashboard);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Dashboard $dashboard
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Dashboard $dashboard)
    {
        $this->authorize('admin.dashboard.edit', $dashboard);


        return view('admin.dashboard.edit', [
            'dashboard' => $dashboard,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDashboard $request
     * @param Dashboard $dashboard
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateDashboard $request, Dashboard $dashboard)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Dashboard
        $dashboard->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/dashboards'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/dashboards');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyDashboard $request
     * @param Dashboard $dashboard
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyDashboard $request, Dashboard $dashboard)
    {
        $dashboard->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyDashboard $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyDashboard $request): Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Dashboard::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
