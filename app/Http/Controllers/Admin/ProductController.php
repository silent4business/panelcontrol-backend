<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Product\BulkDestroyProduct;
use App\Http\Requests\Admin\Product\DestroyProduct;
use App\Http\Requests\Admin\Product\IndexProduct;
use App\Http\Requests\Admin\Product\StoreProduct;
use App\Http\Requests\Admin\Product\UpdateProduct;
use App\Models\Product;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Stripe\Exception\ApiErrorException;
use Stripe\StripeClient;
use Stripe\Price;

class ProductController extends Controller
{
    protected $stripe;

    public function __construct()
    {
        $this->stripe = new StripeClient(env('STRIPE_SECRET'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexProduct $request
     * @return array|Factory|View
     */
    public function index(IndexProduct $request)
    {

        $products = $this->stripe->products->all();
        foreach ($products as $product) {
            Product::updateOrCreate(
                ['product_id' => $product->id],
                [
                    'active' => $product->active,
                    'created' => Carbon::parse($product->created)->format('Y-m-d H:i:s'),
                    'default_price' => $product->default_price,
                    'description' => $product->description,
                    'features' => $product->features,
                    'images' => $product->images,
                    'livemode' => $product->livemode,
                    'metadata' => $product->metadata,
                    'name' => $product->name,
                    'object' => $product->object,
                    'package_dimensions' => $product->package_dimensions,
                    'product_id' => $product->id,
                    'shippable' => $product->shippable,
                    'statement_descriptor' => $product->statement_descriptor,
                    'tax_code' => $product->tax_code,
                    'unit_label' => $product->unit_label,
                    'updated' => Carbon::parse($product->updated)->format('Y-m-d H:i:s'),
                    'url' => $product->url,
                ]
            );
        }
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Product::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['active', 'created', 'default_price', 'description', 'features', 'id', 'images', 'livemode', 'metadata', 'name', 'object', 'package_dimensions', 'product_id', 'shippable', 'statement_descriptor', 'tax_code', 'unit_label', 'updated', 'url'],

            // set columns to searchIn
            ['default_price', 'description', 'features', 'id', 'images', 'metadata', 'name', 'object', 'package_dimensions', 'product_id', 'statement_descriptor', 'tax_code', 'unit_label', 'url']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.product.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.product.create');

        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProduct $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreProduct $request)
    {
        // Función para convertir cadena a slug
        function joinWithHyphen($cadena)
        {
            return strtolower(str_replace(' ', '-', $cadena));
        }

        // Obtiene todas las imágenes y toma la última
        $image = '';
        foreach ($request->images as $value) {
            $image = $value;
        }

        // Crea el producto en Stripe
        $product = $this->stripe->products->create([
            'name' => $request->name,
            'description' => $request->description,
            'metadata' => [
                'description' => $request->description,
                'img' => $image,
                'name' => $request->name,
                'slug' => joinWithHyphen($request->name)
            ]
        ]);

        // Obtiene y limpia el precio por defecto
        $default_price = preg_replace('/[^0-9]/', '', $request->input('default_price'));
        $default_price_int = (int) $default_price;

        // Mapea las opciones de periodo a intervalos de facturación
        $intervalMapping = [
            '1_month' => ['interval' => 'month', 'interval_count' => 1],
            '3_months' => ['interval' => 'month', 'interval_count' => 3],
            '6_months' => ['interval' => 'month', 'interval_count' => 6],
            'yearly' => ['interval' => 'year', 'interval_count' => 1]
        ];


        // Crea el precio en Stripe usando el mapeo de intervalo
        $intervalSelection = $request->object;
        $price = $this->stripe->prices->create([
            'unit_amount' => $default_price_int,
            'currency' => $request->tax_code,
            'recurring' => $intervalMapping[$intervalSelection],
            'product' => $product->id,
        ]);

        $this->stripe->products->update(
            $product->id,
            ['default_price' => $price->id]
        );
        // Crea el producto en la base de datos local (si es necesario)
        $sanitized = $request->getSanitized();
        $products = $this->stripe->products->all();
        foreach ($products as $product) {
            Product::updateOrCreate(
                ['product_id' => $product->id],
                [
                    'active' => $product->active,
                    'created' => Carbon::parse($product->created)->format('Y-m-d H:i:s'),
                    'default_price' => $product->default_price,
                    'description' => $product->description,
                    'features' => $product->features,
                    'images' => $product->images,
                    'livemode' => $product->livemode,
                    'metadata' => $product->metadata,
                    'name' => $product->name,
                    'object' => $product->object,
                    'package_dimensions' => $product->package_dimensions,
                    'product_id' => $product->id,
                    'shippable' => $product->shippable,
                    'statement_descriptor' => $product->statement_descriptor,
                    'tax_code' => $product->tax_code,
                    'unit_label' => $product->unit_label,
                    'updated' => Carbon::parse($product->updated)->format('Y-m-d H:i:s'),
                    'url' => $product->url,
                ]
            );
        }

        // Redirecciona según sea una solicitud AJAX o tradicional
        if ($request->ajax()) {
            return ['redirect' => url('admin/products'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/products');
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @throws AuthorizationException
     * @return void
     */
    public function show(Product $product)
    {
        $this->authorize('admin.product.show', $product);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Product $product)
    {
        $this->authorize('admin.product.edit', $product);


        return view('admin.product.edit', [
            'product' => $product,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProduct $request
     * @param Product $product
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateProduct $request, Product $product)
    {
        // Obtiene todas las imágenes y toma la última
        $image = '';
        foreach ($request->images as $value) {
            $image = $value;
        }
        function joinWithHyphens($cadena)
        {
            return strtolower(str_replace(' ', '-', $cadena));
        }


        $product_id = $product->product_id;
        $price_id = $product->default_price;
        $this->stripe->products->update( $product_id,
            [ 'name' => $request->name,
            'description' => $request->description,
            'metadata' => [
                'description' => $request->description,
                'img' => $image,
                'name' => $request->name,
                'slug' => joinWithHyphens($request->name)
            ]
            ]
        );

        // Obtiene y limpia el precio por defecto
        $default_price = preg_replace('/[^0-9]/', '', $request->input('default_price'));
        $default_price_int = (int) $default_price;

        // Mapea las opciones de periodo a intervalos de facturación
        $intervalMapping = [
            '1_month' => ['interval' => 'month', 'interval_count' => 1],
            '3_months' => ['interval' => 'month', 'interval_count' => 3],
            '6_months' => ['interval' => 'month', 'interval_count' => 6],
            'yearly' => ['interval' => 'year', 'interval_count' => 1]
        ];


        // Crea el precio en Stripe usando el mapeo de intervalo
        $intervalSelection = $request->object;

        $currentPrice = $this->stripe->prices->retrieve($price_id);

        // Crear un nuevo precio en Stripe con el nuevo monto
        $newPrice = $this->stripe->prices->create([
            'unit_amount' => $default_price_int,
            'currency' => 'mxn',
            'recurring' => $intervalMapping[$intervalSelection],
            'product' => $product_id,
        ]);

        $this->stripe->products->update(
            $request->input('product_id'),
            ['default_price' => $newPrice->id]
        );

        // Desactivar el precio antiguo
        $this->stripe->prices->update($price_id, ['active' => false]);

        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Product
        $products = $this->stripe->products->all();
        foreach ($products as $product) {
            Product::updateOrCreate(
                ['product_id' => $product->id],
                [
                    'active' => $product->active,
                    'created' => Carbon::parse($product->created)->format('Y-m-d H:i:s'),
                    'default_price' => $product->default_price,
                    'description' => $product->description,
                    'features' => $product->features,
                    'images' => $product->images,
                    'livemode' => $product->livemode,
                    'metadata' => $product->metadata,
                    'name' => $product->name,
                    'object' => $product->object,
                    'package_dimensions' => $product->package_dimensions,
                    'product_id' => $product->id,
                    'shippable' => $product->shippable,
                    'statement_descriptor' => $product->statement_descriptor,
                    'tax_code' => $product->tax_code,
                    'unit_label' => $product->unit_label,
                    'updated' => Carbon::parse($product->updated)->format('Y-m-d H:i:s'),
                    'url' => $product->url,
                ]
            );
        }

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/products'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyProduct $request
     * @param Product $product
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyProduct $request, Product $product)
    {
        try {
            // Obtener el ID del producto desde el modelo Eloquent
            $product_id = $product->product_id;

            // Obtener y archivar todos los precios asociados al producto en Stripe
            $prices = $this->stripe->products->update(
                $product_id,
                ['active' => false]
            );

            // Eliminar el producto localmente en tu base de datos
            $product->active = false;
            $product->save();

            // Respuesta exitosa para peticiones Ajax
            if ($request->ajax()) {
                return response()->json(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
            }

            // Redireccionar de regreso después de eliminar
            return redirect()->back();
        } catch (ApiErrorException $e) {
            // Manejar errores de la API de Stripe
            return response()->json(['error' => $e->getMessage()], $e->getHttpStatus());
        }
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyProduct $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyProduct $request): Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Product::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
