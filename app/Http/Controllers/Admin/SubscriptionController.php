<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Subscription\BulkDestroySubscription;
use App\Http\Requests\Admin\Subscription\DestroySubscription;
use App\Http\Requests\Admin\Subscription\IndexSubscription;
use App\Http\Requests\Admin\Subscription\StoreSubscription;
use App\Http\Requests\Admin\Subscription\UpdateSubscription;
use App\Models\Subscription;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class SubscriptionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexSubscription $request
     * @return array|Factory|View
     */
    public function index(IndexSubscription $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Subscription::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            [''],

            // set columns to searchIn
            ['']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.subscription.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.subscription.create');

        return view('admin.subscription.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSubscription $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreSubscription $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Subscription
        $subscription = Subscription::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/subscriptions'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/subscriptions');
    }

    /**
     * Display the specified resource.
     *
     * @param Subscription $subscription
     * @throws AuthorizationException
     * @return void
     */
    public function show(Subscription $subscription)
    {
        $this->authorize('admin.subscription.show', $subscription);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Subscription $subscription
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Subscription $subscription)
    {
        $this->authorize('admin.subscription.edit', $subscription);


        return view('admin.subscription.edit', [
            'subscription' => $subscription,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSubscription $request
     * @param Subscription $subscription
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateSubscription $request, Subscription $subscription)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Subscription
        $subscription->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/subscriptions'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/subscriptions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroySubscription $request
     * @param Subscription $subscription
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroySubscription $request, Subscription $subscription)
    {
        $subscription->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroySubscription $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroySubscription $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Subscription::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
