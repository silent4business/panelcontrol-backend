<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAcquisitionRequest;
use App\Http\Resources\AcquisitionResource;
use App\Services\AcquisitionService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @OA\Tag(
 *      name="Adquisiciones",
 *      description="Operaciones de adquisiciones V1.0"
 * )
 */
class AcquisitionController extends Controller
{

    private $acquisitionService;

    public function __construct(AcquisitionService $acquisitionService)
    {
        $this->acquisitionService = $acquisitionService;
    }
    /**
     * @OA\Get(
     *      path="api/acquisitions",
     *      summary="Metodo para listar adquisiciones registradas",
     *      tags={"Adquisiciones"},
     *      description="Get all Acquisitions",
     *      @OA\Response(
     *          response=200,
     *          description="",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Acquisition")
     *              ),
     *          )
     *      )
     * )
     */
    public function index()
    {
        return $this->dataResponse(
            AcquisitionResource::collection(
                $this->acquisitionService->showAllAcquisition()
            )
        );
    }

    /**
     * @OA\Post(
     *      path="/api/acquisitions",
     *      summary="Metodo para crear adquisiciones",
     *      tags={"Adquisiciones"},
     *      description="Create Acquisition",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="module_id", type="string"),
     *              @OA\Property(property="proyect_id", type="string"),
     *              @OA\Property(property="cliente_id", type="string"),
     *              @OA\Property(property="activation_date", type="date"),
     *              @OA\Property(property="expiration_date", type="date"),
     *              @OA\Property(property="is_active", type="boolean"),
     *          )
     *      ),
     *      @OA\Response(
     *              response="200",
     *              description="Acquisition created sucessfuly",
     *              @OA\JsonContent(
     *                  type="object",
     *                  @OA\Property(
     *                      property="status",
     *                      type="string"
     *                  ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *                  ),
     *              )
     *          )
     * )
     *
     * @param CreateUserRequest $request
     * @return void
     */
    public function store(CreateAcquisitionRequest $request): Response
    {
        return $this->basicResponse(
            $this->acquisitionService->createAcquisition(
                $request->proyect_id,
                $request->cliente_id,
                $request->input('acquisition_modules')
            )
        );
    }

    /**
     * @OA\Get(
     *      path="/api/acquisitions/{id}",
     *      summary="Metodo para obtener una adquisicion por id",
     *      tags={"Adquisiciones"},
     *      description="Get Acquisition",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of Acquisition",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Acquisition")
     *              ),
     *          )
     *      )
     * )
     */
    public function show(string $id): Response
    {
        return $this->basicResponse(
            new AcquisitionResource(
                $this->acquisitionService->getOne($id)
            )
        );
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id): Response
    {
        return $this->basicResponse('');
    }

    /**
     * @OA\Put(
     *      path="/api/acquisitions/{id}",
     *      summary="Actualizar adquisicion",
     *      tags={"Adquisiciones"},
     *      description="Update Acquisition",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of Acquisition",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\RequestBody(
     *        required=true,
     *       @OA\JsonContent(
     *              @OA\Property(property="module_id", type="string"),
     *              @OA\Property(property="proyect_id", type="string"),
     *              @OA\Property(property="cliente_id", type="string"),
     *              @OA\Property(property="activation_date", type="date"),
     *              @OA\Property(property="expiration_date", type="date"),
     *              @OA\Property(property="is_active", type="boolean"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Acquisition updated success",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *          )
     *      )
     * )
     */
    public function update(Request $request, string $id): Response
    {
        return $this->basicResponse(
            $this->acquisitionService->updateAcquisition(
                $id,
                $request->proyect_id,
                $request->cliente_id,
                $request->input('acquisition_modules')
            )
        );
    }

    /**
     * @OA\Delete(
     *      path="/api/acquisitions/{id}",
     *      summary="Metodo para eliminar una adquisicion",
     *      tags={"Adquisiciones"},
     *      description="Delete Acquisition",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of Acquisition",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Acquisition deleted successfully",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *          )
     *      )
     * )
     */
    public function destroy(string $id): Response
    {
        return $this->basicResponse(
            $this->acquisitionService->deleteAcquisition($id)
        );
    }

    /**
     * 
     */
    public function status(int $id): Response
    {
        return $this->basicResponse(
            $this->acquisitionService->updateStatus($id)
        );
    }
}
