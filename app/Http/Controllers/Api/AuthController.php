<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\UserResource;
use App\Services\AuthService;

/**
 * @OA\Tag(
 *      name="Auth",
 *      description="Operaciones de autenticacion de los usuarios V1.0"
 * )
 */
class AuthController extends Controller
{
    private  $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService =  $authService;
    }

    /**
     * @OA\Post(
     *      path="/api/auth/login",
     *      summary="Metodo para iniciar sesion en el api",
     *      tags={"Auth"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="email", type="string", example="example@test.com"),
     *              @OA\Property(property="password", type="string")
     *          )
     *      ),
     *      @OA\Response(response="200", description="Welcome")
     * )
     *
     * @param LoginRequest $request
     * @return void
     */
    public function loginUser(LoginRequest $request)
    {
        return $this->dataResponse([
            'token' =>  $this->authService->signin(
                $request->input('email'),
                $request->input('password')
            ),
            'message' => "Welcomme back :)!",
            'user' => new UserResource(
                $this->authService->me(
                    $request->input('email')
                )
            )
        ]);
    }

    /**
     * @OA\Get(
     *      path="/api/auth/logout",
     *      tags={"Auth"},
     *      summary="Metodo para cerrar sesion",
     *      @OA\Response(response="200", description="See you")
     * )
     *
     * @return void
     */
    public function logout()
    {
        return $this->basicResponse(
            $this->authService->signout()
        );
    }
}
