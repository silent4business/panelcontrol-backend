<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CreateClienteRequest;
use App\Http\Requests\Api\UpdateClienteRequest;
use App\Services\ClientService;
use Symfony\Component\HttpFoundation\Response;

/**
 * @OA\Tag(
 *      name="Clientes",
 *      description="Operaciones de autenticacion de los usuarios V1.0"
 * )
 */
class ClienteController extends Controller
{

    private $clientService;

    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    /**
     * @OA\Get(
     *      path="/api/clients",
     *      summary="Metodo para listar clientes registrados",
     *      tags={"Clientes"},
     *      description="Get all Clients",
     *      @OA\Response(
     *          response=200,
     *          description="",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Client")
     *              ),
     *          )
     *      )
     * )
     */
    public function index(): Response
    {
        return $this->dataResponse(
            $this->clientService->showAllClient()
        );
    }

    /**
     * @OA\Post(
     *      path="/api/clients",
     *      summary="Metodo para crear cliente",
     *      tags={"Clientes"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string"),
     *              @OA\Property(property="email", type="string"),
     *              @OA\Property(property="domicilio", type="string"),
     *              @OA\Property(property="razon_social", type="string"),
     *              @OA\Property(property="contacto", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *              response="200",
     *              description="Client created sucessfuly",
     *              @OA\JsonContent(
     *                  type="object",
     *                  @OA\Property(
     *                      property="status",
     *                      type="string"
     *                  ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *                  ),
     *              )
     * )
     * )
     *
     * @param CreateClienteRequest $request
     * @return void
     */
    public function store(CreateClienteRequest $request): Response
    {
        return $this->basicResponse(
            $this->clientService->createClient($request->all())
        );
    }

    /**
     * @OA\Get(
     *      path="/api/clients/{id}",
     *      summary="Metodo para obtener un cliente por id",
     *      tags={"Clientes"},
     *      description="Get Client",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of Client",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Client")
     *              ),
     *          )
     *      )
     * )
     */
    public function show(int $id): Response
    {
        return $this->dataResponse(
            $this->clientService->findOneClient($id)
        );
    }

    /**
     * @OA\Put(
     *      path="/api/clients/{id}",
     *      summary="Actualizar cliente",
     *      tags={"Clientes"},
     *      description="Update Cliente",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of client",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\RequestBody(
     *        required=true,
     *       @OA\JsonContent(
     *              @OA\Property(property="name", type="string"),
     *              @OA\Property(property="email", type="string"),
     *              @OA\Property(property="domicilio", type="string"),
     *              @OA\Property(property="razon_social", type="string"),
     *              @OA\Property(property="contacto", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Client updated success",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *              ),
     *          )
     *      )
     * )
     */
    public function update(UpdateClienteRequest $request, int $id): Response
    {
        return $this->basicResponse(
            $this->clientService->updateClient($id,$request->all())
        );
    }

    /**
     * @OA\Delete(
     *      path="/api/clients/{id}",
     *      summary="Metodo para eliminar cliente",
     *      tags={"Clientes"},
     *      description="Delete Client",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of Client",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Client deleted successfully",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *          )
     *      )
     * )
     */
    public function destroy(string $id): Response
    {
        return $this->basicResponse(
            $this->clientService->deleteClient($id)
        );
    }
}
