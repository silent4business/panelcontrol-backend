<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CreateClienteModuloRequest;
use App\Http\Requests\Api\UpdateClienteModuloRequest;
use App\Http\Resources\ClienteModuloResource;
use App\Models\ClienteModulo;
use Illuminate\Http\Request;

class ClienteModuloController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $cliente_modulo = ClienteModulo::with('cliente','modulo.proyecto')->get();
        return response()->json(['data'=>ClienteModuloResource::collection($cliente_modulo)]);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateClienteModuloRequest $request)
    {
        $moduls = $request->input('moduls');

        foreach($moduls as $modul){

            ClienteModulo::create($modul);
        }

         return response()->json('purchase service saved successfully',200);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $cliente_modulo = ClienteModulo::with('cliente','modulo.proyecto')->find($id);
        return response()->json(['data'=>new ClienteModuloResource($cliente_modulo)]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateClienteModuloRequest $request, $id)
    {
        $input = $request->all();
        $cliente_modulo = ClienteModulo::find($id);
        if (empty($cliente_modulo)) {
            return response()->json('purchase service not found',404);
        }
        try {
            $cliente_modulo->update($input);
            return response()->json('purchase service updated successfully',200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $cliente_modulo = ClienteModulo::find($id);
        if (empty($cliente_modulo)) {
            return response()->json('purchase service not found',404);
        }

        $cliente_modulo->delete();
        return response()->json('purchase service deleted successfully',200);


    }
}
