<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ModuloResource;
use App\Services\ModuleService;


/**
 * @OA\Tag(
 *      name="Modulos",
 *      description="Operaciones de modulos V1.0"
 * )
 */
class ModulesController extends Controller
{
    private $moduleService;

    public function __construct(ModuleService $moduleService)
    {
        $this->moduleService = $moduleService;
    }

    public function index()
    {
        return $this->dataResponse(
            // ModuloResource::collection(
            //     $this->moduleService->getAll()
            // )
            ""
        );
    }

    public function buyModule($project_id, $module_id)
    {
        return $this->basicResponse(
            $this->moduleService->buyModule($project_id, $module_id)
        );
    }

    /**
     * @OA\Delete(
     *      path="/api/module/{id}",
     *      summary="Metodo para eliminar un modulo",
     *      tags={"Modulos"},
     *      description="Delete Module",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of Module",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Module deleted successfully",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *          )
     *      )
     * )
     */
    public function destroy(int $id){
        return $this->basicResponse(
        $this->moduleService->deleteModule($id)
        );
    }

}
