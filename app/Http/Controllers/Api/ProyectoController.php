<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CreateProyectoRequest;
use App\Http\Resources\AllProyectoResource;
use App\Services\ProyectService;
use Illuminate\Http\Request;

/**
 * @OA\Tag(
 *      name="Proyectos",
 *      description="Operaciones de proyectos V1.0"
 * )
 */
class ProyectoController extends Controller
{
    private $proyectService;

    public function __construct(ProyectService $proyectService)
    {
        $this->proyectService = $proyectService;
    }

    /**
     * @OA\Get(
     *      path="/api/projects",
     *      summary="Metodo para listar proyectos registrados",
     *      tags={"Proyectos"},
     *      description="Get all Proyects",
     *      @OA\Response(
     *          response=200,
     *          description="",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Proyect")
     *              ),
     *          )
     *      )
     * )
     */
    public function index()
    {
        return $this->dataResponse(
            AllProyectoResource::collection(
                $this->proyectService->getAll()
            )
        );
    }

    /**
     * @OA\Get(
     *      path="/api/projects/{id}",
     *      summary="Metodo para obtener un proyecto por id",
     *      tags={"Proyectos"},
     *      description="Get Proyect",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of Proyect",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Proyect")
     *              ),
     *          )
     *      )
     * )
     */
    public function show(int $id)
    {
        return $this->dataResponse(
            new AllProyectoResource(
                $this->proyectService->findById($id)
            )
        );
    }

    /**
     * @OA\Post(
     *      path="/api/projects",
     *      summary="Metodo para crear proyectos",
     *      tags={"Proyectos"},
     *      description="Create Proyect",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                           property="title",
     *                           type="string"),
     *              @OA\Property(
     *                           property="url_base",
     *                           type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *              response="200",
     *              description="Proyect created sucessfuly",
     *              @OA\JsonContent(
     *                  type="object",
     *                  @OA\Property(
     *                      property="status",
     *                      type="string"
     *                  ),
        *              @OA\Property(
        *                  property="message",
        *                  type="string"
     *                  ),
     *              )
     *          )
     * )
     *
     * @param CreateUserRequest $request
     * @return void
     */
    public function store(CreateProyectoRequest $request)
    {
        return $this->basicResponse(

            $this->proyectService->createProyect(
                $request->input('modules'),
                $request->title,
                $request->url_base
            )
        );
    }

    /**
     * @OA\Put(
     *      path="/api/projects/{id}",
     *      summary="Actualizar proyecto",
     *      tags={"Proyectos"},
     *      description="Update Proyect",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of Proyect",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\RequestBody(
     *        required=true,
     *       @OA\JsonContent(
     *              @OA\Property(property="title", type="string"),
     *              @OA\Property(property="url_base", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Proyect updated success",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                           property="title",
     *                           type="string"),
     *              @OA\Property(
     *                           property="url_base",
     *                           type="string"),
     *          )
     *      )
     * )
     */
    public function update(Request $request, int $id)
    {
        return $this->basicResponse(
            $this->proyectService->updateProyect(
                $request->input('modules'),
                $request->title,
                $request->url_base,
                $id
            )
        );
    }

    /**
     * @OA\Delete(
     *      path="/api/projects/{id}",
     *      summary="Metodo para eliminar proyecto",
     *      tags={"Proyectos"},
     *      description="Delete Proyect",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of Proyect",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Proyect deleted successfully",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *          )
     *      )
     * )
     */
    public function destroy(int $id)
    {
        return $this->basicResponse(
            $this->proyectService->deleteProyect($id)
        );
    }

    public function validateAdquisitions($project_id, $module_id)
    {
        return $this->dataResponse(
            $this->proyectService->validateModules($project_id, $module_id)
        );
    }

}
