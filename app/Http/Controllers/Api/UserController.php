<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Services\UserService;
/**
 * @OA\Tag(
 *      name="Usuarios",
 *      description="Operaciones de usuarios V1.0"
 * )
 */
class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @OA\Get(
     *      path="/api/users",
     *      summary="Metodo para listar usuarios registrados",
     *      tags={"Usuarios"},
     *      description="Get all Users",
     *      @OA\Response(
     *          response=200,
     *          description="",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/User")
     *              ),
     *          )
     *      )
     * )
     */
    public function index()
    {
        return $this->dataResponse(
            $this->userService->showAllUser()
        );
    }

    /**
     * @OA\Post(
     *      path="/api/users",
     *      summary="Metodo para crear usuarios",
     *      tags={"Usuarios"},
     *      description="Create User",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string"),
     *              @OA\Property(property="email", type="string"),
     *              @OA\Property(property="domicilio", type="string"),
     *              @OA\Property(property="razon_social", type="string"),
     *              @OA\Property(property="contacto", type="string"),
     *              @OA\Property(property="password", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *              response="200",
     *              description="User created sucessfuly",
     *              @OA\JsonContent(
     *                  type="object",
     *                  @OA\Property(
     *                      property="status",
     *                      type="string"
     *                  ),
        *              @OA\Property(
        *                  property="message",
        *                  type="string"
     *                  ),
     *              )
     *          )
     * )
     *
     * @param CreateUserRequest $request
     * @return void
     */
    public function store(CreateUserRequest $request)
    {
        return $this->basicResponse(
            $this->userService->createUser($request->all())
        );
    }

    /**
     * @OA\Get(
     *      path="/api/users/{id}",
     *      summary="Metodo para obtener un usuario por id",
     *      tags={"Usuarios"},
     *      description="Get User",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of User",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/User")
     *              ),
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        return $this->dataResponse(
            $this->userService->findOneUSer($id)
        );
    }

    /**
     * @OA\Put(
     *      path="/api/users/{id}",
     *      summary="Actualizar usuario",
     *      tags={"Usuarios"},
     *      description="Update User",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of user",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\RequestBody(
     *        required=true,
     *       @OA\JsonContent(
     *              @OA\Property(property="name", type="string"),
     *              @OA\Property(property="email", type="string"),
     *              @OA\Property(property="domicilio", type="string"),
     *              @OA\Property(property="razon_social", type="string"),
     *              @OA\Property(property="contacto", type="string"),
     *              @OA\Property(property="password", type="string"),
     *
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="User updated success",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *          )
     *      )
     * )
     */
    public function update(UpdateUserRequest $request, $id)
    {
        return $this->basicResponse(
            $this->userService->updateUser($id,$request->all())
        );

    }

    /**
     * @OA\Delete(
     *      path="/api/users/{id}",
     *      summary="Metodo para eliminar usuario",
     *      tags={"Usuarios"},
     *      description="Delete User",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of User",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="User deleted successfully",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="status",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        return $this->basicResponse(
            $this->userService->deleteUser($id)
        );
    }
}
