<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(title="Api panel de control", version="1.0.0")
 * @OA\Server(url="http://back.test")
 * 
 * @OA\SecurityScheme(
 *      type="http",
 *      scheme="bearer",
 *      securityScheme="bearerAuth",
 *      bearerFormat="JWT"
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function basicResponse($message)
    {
        return response()->json([
            'status' => 'success',
            'message' => $message
        ]);
    }

    public function dataResponse($data)
    {
        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }
}
