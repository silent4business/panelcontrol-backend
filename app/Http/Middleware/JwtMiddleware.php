<?php

namespace App\Http\Middleware;

use App\Exceptions\UnauthorizedException;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class JwtMiddleware
{
    /**
     * Este middleware valida si el usuario esta autenticado o no
     * almacena los datos del usuario en el request para poder ser
     * usados posteriormente.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        try{
            $request['user'] = JWTAuth::parseToken()->authenticate();
        } catch(JWTException $e) {
            throw new UnauthorizedException("Unautorized");
        }
        return $next($request);
    }
}
