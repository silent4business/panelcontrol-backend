<?php

namespace App\Http\Requests\Admin\Coupon;

use Brackets\Translatable\TranslatableFormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreCoupon extends TranslatableFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.coupon.create');
    }

/**
     * Get the validation rules that apply to the requests untranslatable fields.
     *
     * @return array
     */
    public function untranslatableRules(): array {
        return [
            'amount_off' => ['nullable', 'integer'],
            'coupons_id' => ['nullable', 'string'],
            'created' => ['nullable', 'integer'],
            'currency' => ['nullable', 'string'],
            'duration' => ['nullable', 'string'],
            'duration_in_months' => ['nullable', 'integer'],
            'livemode' => ['nullable', 'boolean'],
            'max_redemptions' => ['nullable', 'integer'],
            'name' => ['nullable', 'string'],
            'object' => ['nullable', 'string'],
            'percent_off' => ['nullable', 'numeric'],
            'redeem_by' => ['nullable', 'integer'],
            'times_redeemed' => ['nullable', 'integer'],
            'valid' => ['nullable', 'boolean'],

        ];
    }

    /**
     * Get the validation rules that apply to the requests translatable fields.
     *
     * @return array
     */
    public function translatableRules($locale): array {
        return [
            'metadata' => ['nullable', 'string'],

        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
