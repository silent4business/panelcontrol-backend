<?php

namespace App\Http\Requests\Admin\Dashboard;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreDashboard extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.dashboard.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'subscription_id' => ['required', 'string'],
            'nombre' => ['required', 'string'],
            'servicio' => ['required', 'string'],
            'status' => ['required', 'string'],
            'currency' => ['required', 'string'],
            'object' => ['required', 'string'],
            'interval' => ['required', 'string'],
            'interval_count' => ['required', 'string'],
            'current_period_end' => ['required', 'string'],
            'current_period_start' => ['required', 'string'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
