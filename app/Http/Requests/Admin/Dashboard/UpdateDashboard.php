<?php

namespace App\Http\Requests\Admin\Dashboard;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateDashboard extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.dashboard.edit', $this->dashboard);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'subscription_id' => ['sometimes', 'string'],
            'nombre' => ['sometimes', 'string'],
            'servicio' => ['sometimes', 'string'],
            'status' => ['sometimes', 'string'],
            'currency' => ['sometimes', 'string'],
            'object' => ['sometimes', 'string'],
            'interval' => ['sometimes', 'string'],
            'interval_count' => ['sometimes', 'string'],
            'current_period_end' => ['sometimes', 'string'],
            'current_period_start' => ['sometimes', 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
