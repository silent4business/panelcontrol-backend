<?php

namespace App\Http\Requests\Admin\Product;

use Brackets\Translatable\TranslatableFormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateProduct extends TranslatableFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.product.edit', $this->product);
    }

/**
     * Get the validation rules that apply to the requests untranslatable fields.
     *
     * @return array
     */
    public function untranslatableRules(): array {
        return [
            'active' => ['nullable', 'boolean'],
            'created' => ['nullable', 'date'],
            'default_price' => ['nullable', 'string'],
            'description' => ['nullable', 'string'],
            'livemode' => ['nullable', 'boolean'],
            'name' => ['required', 'string'],
            'object' => ['nullable', 'string'],
            'product_id' => ['nullable', 'string'],
            'shippable' => ['nullable', 'boolean'],
            'statement_descriptor' => ['nullable', 'string'],
            'tax_code' => ['nullable', 'string'],
            'unit_label' => ['nullable', 'string'],
            'updated' => ['nullable', 'date'],
            'url' => ['nullable', 'string'],


        ];
    }

    /**
     * Get the validation rules that apply to the requests translatable fields.
     *
     * @return array
     */
    public function translatableRules($locale): array {
        return [
            'features' => ['nullable', 'string'],
            'images' => ['nullable', 'string'],
            'metadata' => ['nullable', []],
            'package_dimensions' => ['nullable', 'string'],
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
