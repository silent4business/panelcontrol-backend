<?php

namespace App\Http\Requests\Admin\Promotion;

use Brackets\Translatable\TranslatableFormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StorePromotion extends TranslatableFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.promotion.create');
    }

/**
     * Get the validation rules that apply to the requests untranslatable fields.
     *
     * @return array
     */
    public function untranslatableRules(): array {
        return [
            'active' => ['required', 'boolean'],
            'code' => ['required', 'string'],
            'coupon_id' => ['required', 'string'],
            'created' => ['nullable', 'date'],
            'customer' => ['nullable', 'string'],
            'expires_at' => ['nullable', 'date'],
            'livemode' => ['required', 'boolean'],
            'max_redemptions' => ['nullable', 'integer'],
            'object' => ['required', 'string'],
            'promotion_id' => ['required', 'string'],
            'times_redeemed' => ['nullable', 'integer'],
            
        ];
    }

    /**
     * Get the validation rules that apply to the requests translatable fields.
     *
     * @return array
     */
    public function translatableRules($locale): array {
        return [
            'metadata' => ['required', 'string'],
            'restrictions' => ['required', 'string'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
