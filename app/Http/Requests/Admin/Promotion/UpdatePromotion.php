<?php

namespace App\Http\Requests\Admin\Promotion;

use Brackets\Translatable\TranslatableFormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdatePromotion extends TranslatableFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.promotion.edit', $this->promotion);
    }

/**
     * Get the validation rules that apply to the requests untranslatable fields.
     *
     * @return array
     */
    public function untranslatableRules(): array {
        return [
            'active' => ['sometimes', 'boolean'],
            'code' => ['sometimes', 'string'],
            'coupon_id' => ['sometimes', 'string'],
            'created' => ['nullable', 'date'],
            'customer' => ['nullable', 'string'],
            'expires_at' => ['nullable', 'date'],
            'livemode' => ['sometimes', 'boolean'],
            'max_redemptions' => ['nullable', 'integer'],
            'object' => ['sometimes', 'string'],
            'promotion_id' => ['sometimes', 'string'],
            'times_redeemed' => ['nullable', 'integer'],
            

        ];
    }

    /**
     * Get the validation rules that apply to the requests translatable fields.
     *
     * @return array
     */
    public function translatableRules($locale): array {
        return [
            'metadata' => ['sometimes', 'string'],
            'restrictions' => ['sometimes', 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
