<?php

namespace App\Http\Requests\Api;

use App\Models\Proyecto;
use Illuminate\Foundation\Http\FormRequest;

class CreateProyectoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
        'title' => 'required|string|max:255',
        'url_base' => 'required|string|max:255',
        'modules' => 'required|array|max:255',
        'modules.*.title' => 'string|max:100',
        'modules.*.url'=> 'string|max:255'
    ];
    }
}
