<?php

namespace App\Http\Requests\Api;

use App\Models\ClienteModulo;
use Illuminate\Foundation\Http\FormRequest;

class UpdateClienteModuloRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'cliente_id' => 'required|integer',
            'modulo_id' => 'required|integer',
            'fecha_activacion' => 'required|date',
            'fecha_finalizacion' => 'required|date',
        ];
    }
}
