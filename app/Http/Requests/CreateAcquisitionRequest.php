<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAcquisitionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'proyect_id' => 'required|int',
            'cliente_id' => 'required|int',
            'acquisition_modules' => 'required|array|max:255',
            'acquisition_modules.*.module_id' => 'int|max:100',
            'acquisition_modules.*.activation_date'=> 'string|max:255',
            'acquisition_modules.*.expiration_date'=> 'string|max:255',

        ];
    }
}
