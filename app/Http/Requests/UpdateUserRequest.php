<?php

namespace App\Http\Requests;


class UpdateUserRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'domicilio' => 'nullable|string|max:255',
            'razon_social' => 'required|string|max:255',
            'contacto'=> 'required|string|max:100',
        ];
    }
}
