<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AcquisitionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            "id" => $this->id,
            "client_name" => $this->client->name,
            "client_email" => $this->client->email,
            "proyect" => $this->project->title,
            "is_active" => $this->is_active,
            "buy_modules" => AcqusitionsModulesResource::collection($this->acquisitionModule)
        ];
    }
}
