<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AcqusitionsModulesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'is_active' => $this->is_active,
            'activation_date' => $this->activation_date,
            'expiration_date' => $this->expiration_date,
            'module' => new ModuloResource($this->modulo)
        ];
    }
}
