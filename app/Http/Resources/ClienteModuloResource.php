<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ClienteModuloResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=> $this->id,
            'is_active' => $this->is_active,
            'cliente' => new ClienteResource($this->cliente),
            'modulo' => [
                'id' => $this->modulo->id,
                'title' => $this->modulo->title,
                'proyecto' => [
                    'id' => $this->modulo->proyecto->id,
                    'title' => $this->modulo->proyecto->title,
                ],
            ],
        ];
    }
}
