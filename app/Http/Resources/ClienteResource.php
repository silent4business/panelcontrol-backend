<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ClienteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'domicilio' => $this->domicilio,
            'razon_social' => $this->razon_social,
            'contacto' => $this->contacto,
            'representante'=> $this->representante,
            'representante_email'=>$this->representante_email,
            'representante_telefono'=>$this->representante_telefono,
            'consultor'=>$this->consultor,
            'consultor_email'=>$this->consultor_email,
            'consultor_telefono'=>$this->consultor_telefono,
        ];
    }
}
