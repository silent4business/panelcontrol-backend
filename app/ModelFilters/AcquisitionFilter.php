<?php 

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class AcquisitionFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function module($module_id)
    {
        return $this->where('module_id', $module_id);
    }

    public function proyect($proyect_id)
    {
        return $this->where('proyect_id', $proyect_id);
    }

    public function client($client_id)
    {
        return $this->where('client_id', $client_id);
    }

    public function isActive($status = true)
    {
        return $this->where('is_active', $status);
    }
}
