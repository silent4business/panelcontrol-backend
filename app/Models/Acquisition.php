<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
     * @OA\Schema(
     *      schema="Acquisition",
     *      required={"module_id",
     *              "proyect_id","cliente_id",
     *              "activation_date",
     *              "expiration_date",
     *              "is_active"},
     *       @OA\Property(
     *          property="module_id",
     *          description="",
     *          readOnly=false,
     *          nullable=false,
     *          type="string",
     *      ),
     *       @OA\Property(
     *          property="proyect_id",
     *          description="",
     *          readOnly=false,
     *          nullable=false,
     *          type="string",
     *      ),
     *       @OA\Property(
     *          property="cliente_id",
     *          description="",
     *          readOnly=false,
     *          nullable=false,
     *          type="string",
     *      ),
     *       @OA\Property(
     *          property="activation_date",
     *          description="",
     *          readOnly=false,
     *          nullable=false,
     *          type="date",
     *      ),
     *       @OA\Property(
     *          property="expiration_date",
     *          description="",
     *          readOnly=false,
     *          nullable=false,
     *          type="date",
     *      ),
     *       @OA\Property(
     *          property="is_acctive",
     *          description="",
     *          readOnly=false,
     *          nullable=false,
     *          type="boolean",
     *      ),
     * )
     */
class Acquisition extends Model
{
    use HasFactory, Filterable;

    protected $fillable = [
        'id',
        'module_id',
        'proyect_id',
        'cliente_id',
        'activation_date',
        'expiration_date',
        'is_active'
    ];

    public function client()
    {
        return $this->belongsTo(Cliente::class, 'cliente_id');
    }

    public function project()
    {
        return $this->belongsTo(Proyecto::class, 'proyect_id');
    }

    public function acquisitionModule()
    {
        return $this->hasMany(AcquisitionModulo::class, 'acquisition_id');
    }
}
