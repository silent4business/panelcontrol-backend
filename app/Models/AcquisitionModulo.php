<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class AcquisitionModulo extends Model
{
    use HasFactory,SoftDeletes, Filterable;

    protected $table ='acquisition_modulos';

    protected $fillable = [
        'acquisition_id',
        'module_id',
        'is_active',
        'activation_date',
        'expiration_date'
    ];

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class);
    }

    public function modulo()
    {
        return $this->belongsTo(Modulo::class, 'module_id');
    }
}
