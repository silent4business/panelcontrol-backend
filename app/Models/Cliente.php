<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @OA\Schema(
 *      schema="Client",
 *      required={"name","domicilio","razon_social","contacto",
 *                 "email","representante","representante_email",
 *                 "representante_telefono","consultor","consultor_email",
 *                 "consultor_telefono",},
 *      @OA\Property(
 *          property="domicilio",
 *          description="",
 *          readOnly=false,
 *          nullable=false,
 *          type="string",
 *      ),
 * ),
 *      @OA\Property(
 *          property="razon_social",
 *          description="",
 *          readOnly=false,
 *          nullable=false,
 *          type="string",
 *      ),
 *       @OA\Property(
 *          property="contacto",
 *          description="",
 *          readOnly=false,
 *          nullable=false,
 *          type="string",
 *      ),
 *       @OA\Property(
 *          property="email",
 *          description="",
 *          readOnly=false,
 *          nullable=false,
 *          type="string",
 *      ),
 *       @OA\Property(
 *          property="repesentante",
 *          description="",
 *          readOnly=false,
 *          nullable=false,
 *          type="string",
 *      ),
 *       @OA\Property(
 *          property="representante_email",
 *          description="",
 *          readOnly=false,
 *          nullable=false,
 *          type="string",
 *      ),
 *       @OA\Property(
 *          property="representante_telefono",
 *          description="",
 *          readOnly=false,
 *          nullable=false,
 *          type="string",
 *      ),
 *        @OA\Property(
 *          property="consultor",
 *          description="",
 *          readOnly=false,
 *          nullable=false,
 *          type="string",
 *      ),
 *       @OA\Property(
 *          property="consultor_email",
 *          description="",
 *          readOnly=false,
 *          nullable=false,
 *          type="string",
 *      ),
 *       @OA\Property(
 *          property="consultor_telefono",
 *          description="",
 *          readOnly=false,
 *          nullable=false,
 *          type="string",
 *      ),
 */

class Cliente extends Model
{
    use HasFactory, SoftDeletes, Filterable;

        protected $fillable = [
        'name',
        'domicilio',
        'razon_social',
        'contacto',
        'email',
        'representante',
        'representante_email',
        'representante_telefono',
        'consultor',
        'consultor_email',
        'consultor_telefono',
    ];

    protected $casts = [
        'name' => 'string',
        'domicilio' => 'string',
        'razon_social' => 'string',
        'contacto' => 'string',
        'email'=>'string',
        'representante' =>'string',
        'representante_email' =>'string',
        'representante_telefono' =>'string',
        'consultor'=> 'string',
        'consultor_email' =>'string',
        'consultor_telefono' =>'string',
    ];

    public static array $rules = [
        'name' => 'required|string|max:255',
        'domicilio' => 'required|string|max:255',
        'razon_social' => 'required|string|max:255',
        'contacto' => 'required|string|max:10',
        'email'=>'required|string|max:255',
        'representante' =>'nullable|string|max:255',
        'representante_email' =>'nullable|string|max:255',
        'representante_telefono' =>'nullable|string|max:255',
        'consultor'=> 'nullable|string|max:255',
        'consultor_email' =>'nullable|string|max:255',
        'consultor_telefono' =>'nullable|string|max:255',
    ];

    public function modulo () {
        return $this->belongsToMany(Modulo::class,'cliente_modulos')->withPivot('is_active');
    }

    public function cliente_modulo () {
        return $this->hasMany(ClienteModulo::class)->orderBy('id', 'asc');
    }

}
