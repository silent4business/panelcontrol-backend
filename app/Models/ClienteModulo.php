<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClienteModulo extends Model
{
    use HasFactory, SoftDeletes, Filterable;

    protected $fillable = [
        'cliente_id',
        'modulo_id',
        'is_active',
        'fecha_activacion',
        'fecha_finalizacion'
    ];

    protected $casts = [
        'cliente_id' => 'integer',
        'modulo_id' => 'integer',
        'is_active' => 'boolean',
        'fecha_activacion' => 'date',
        'fecha_finalizacion' => 'date'
    ];

    public static array $rules = [
        'moduls' => 'required|array|max:255',
        'moduls.*.cliente_id' => 'required|integer',
        'moduls.*.modulo_id' => 'required|integer',
        'moduls.*.fecha_activacion' => 'required|date',
        'moduls.*.fecha_finalizacion' => 'required|date',
    ];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function modulo()
    {
        return $this->belongsTo(Modulo::class);
    }

}
