<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Brackets\Translatable\Traits\HasTranslations;

class Coupon extends Model
{
use HasTranslations;
    protected $fillable = [
        'amount_off',
        'coupons_id',
        'created',
        'currency',
        'duration',
        'duration_in_months',
        'livemode',
        'max_redemptions',
        'metadata',
        'name',
        'object',
        'percent_off',
        'redeem_by',
        'times_redeemed',
        'valid',
    
    ];
    
    
    protected $dates = [
    
    ];
    // these attributes are translatable
    public $translatable = [
        'metadata',
    
    ];
    public $timestamps = false;
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/coupons/'.$this->getKey());
    }
}
