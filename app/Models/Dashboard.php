<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{
    protected $table = 'dashboard';

    protected $fillable = [
        'subscription_id',
        'nombre',
        'servicio',
        'status',
        'currency',
        'object',
        'interval',
        'interval_count',
        'current_period_end',
        'current_period_start',
    
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/dashboards/'.$this->getKey());
    }
}
