<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modulo extends Model
{
    use HasFactory, SoftDeletes, Filterable;

    protected $fillable = [
        'title',
        'proyecto_id',
        'url'
    ];

    public function proyecto()
    {
        return $this->belongsTo(Proyecto::class);
    }

    public function cliente ()
    {
        return $this->belongsToMany(Cliente::class,'cliente_modulos')->withPivot('is_active');
    }
    public function cliente_modulo ()
    {
        return $this->hasMany(ClienteModulo::class)->orderBy('id', 'asc');
    }

    public function acquisitionmodulo()
    {
        return $this->belongsTo(AcquisitionModulo::class);;
    }
}
