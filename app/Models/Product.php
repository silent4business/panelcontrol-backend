<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Brackets\Translatable\Traits\HasTranslations;

class Product extends Model
{
use HasTranslations;
    protected $table = 'product';

    protected $fillable = [
        'active',
        'created',
        'default_price',
        'description',
        'features',
        'images',
        'livemode',
        'metadata',
        'name',
        'object',
        'package_dimensions',
        'product_id',
        'shippable',
        'statement_descriptor',
        'tax_code',
        'unit_label',
        'updated',
        'url',
    
    ];
    
    
    protected $dates = [
        'created',
        'created_at',
        'updated',
        'updated_at',
    
    ];
    // these attributes are translatable
    public $translatable = [
        'features',
        'images',
        'metadata',
        'package_dimensions',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/products/'.$this->getKey());
    }
}
