<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Brackets\Translatable\Traits\HasTranslations;

class Promotion extends Model
{
use HasTranslations;
    protected $table = 'promotion';

    protected $fillable = [
        'active',
        'code',
        'coupon_id',
        'created',
        'customer',
        'expires_at',
        'livemode',
        'max_redemptions',
        'metadata',
        'object',
        'promotion_id',
        'restrictions',
        'times_redeemed',
    
    ];
    
    
    protected $dates = [
        'created',
        'created_at',
        'expires_at',
        'updated_at',
    
    ];
    // these attributes are translatable
    public $translatable = [
        'metadata',
        'restrictions',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/promotions/'.$this->getKey());
    }
}
