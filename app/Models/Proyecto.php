<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
     * @OA\Schema(
     *      schema="Proyect",
     *      required={"title","url_base"},
     *      @OA\Property(
     *          property="title",
     *          description="",
     *          readOnly=false,
     *          nullable=false,
     *          type="string",
     *      ),
     *       @OA\Property(
     *          property="url_base",
     *          description="",
     *          readOnly=false,
     *          nullable=false,
     *          type="string",
     *      ),
     * )
     */
class Proyecto extends Model
{
    use HasFactory, SoftDeletes, Filterable;

    protected $fillable = [
        'title',
        'url_base',
    ];

    protected $casts = [
        'title' => 'string',
        'url_base' => 'string',
    ];


    public function modulo () {
        return $this->hasMany(Modulo::class)->orderBy('id', 'asc');
    }

    public function client()
    {
        return $this->belongsTo(Cliente::class, 'client_id');
    }

    public function acquisitions()
    {
        return $this->hasMany(Acquisition::class, 'proyect_id');
    }
}
