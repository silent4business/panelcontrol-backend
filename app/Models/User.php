<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
     * @OA\Schema(
     *      schema="User",
     *      required={"name","domicilio",
     *                "razon_social","contacto"
     *                ,"email"},
     *      @OA\Property(
     *          property="name",
     *          description="",
     *          readOnly=false,
     *          nullable=false,
     *          type="string",
     *      ),
     *      @OA\Property(
     *          property="domicilio",
     *          description="",
     *          readOnly=false,
     *          nullable=false,
     *          type="string",
     *      ),
     *      @OA\Property(
     *          property="razon_social",
     *          description="",
     *          readOnly=false,
     *          nullable=false,
     *          type="string",
     *      ),
     *      @OA\Property(
     *          property="contacto",
     *          description="",
     *          readOnly=false,
     *          nullable=false,
     *          type="string",
     *      ),
     *      @OA\Property(
     *          property="email",
     *          description="",
     *          readOnly=false,
     *          nullable=false,
     *          type="string",
     *      ),
     * )
     */
class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes, Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'domicilio',
        'razon_social',
        'contacto',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function clients()
    {
        return $this->belongsToMany(UsersClients::class, 'users_clients');
    }
}
