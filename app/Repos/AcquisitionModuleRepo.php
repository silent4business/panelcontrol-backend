<?php

namespace App\Repos;

use App\Models\AcquisitionModulo;

class AcquisitionModuleRepo extends RepoBase
{
    public function __construct(AcquisitionModulo $model)
    {
        parent::__construct($model);
    }

    public function create($data)
    {
        $acquisitionmodule = $this->getModel();
        return $acquisitionmodule::create($data);
    }
}
