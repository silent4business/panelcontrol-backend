<?php

namespace App\Repos;

use App\Models\Acquisition;

class AcquisitionRepo extends RepoBase
{
    public function __construct(Acquisition $model) {
        parent::__construct($model);
    }

    public function create($data)
    {
        $acquisition = $this->getModel();
        return $acquisition::create($data);
    }
}
