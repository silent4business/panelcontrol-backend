<?php

namespace App\Repos;

use App\Models\Cliente;

class ClientRepo extends RepoBase
{
    public function __construct(Cliente $client)
    {
        parent::__construct($client);
    }

    public function create($data)
    {
        $client = $this->getModel();
        return $client::create($data);
    }
}
