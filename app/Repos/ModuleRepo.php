<?php

namespace App\Repos;

use App\Models\Modulo;

class ModuleRepo extends RepoBase
{
    public function __construct(Modulo $model)
    {
        parent::__construct($model);
    }

    public function create($data)
    {
        return Modulo::insertGetId($data);
    }
}
