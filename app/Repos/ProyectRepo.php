<?php

namespace App\Repos;

use App\Models\Proyecto;
use App\Repos\RepoBase;

class ProyectRepo extends RepoBase
{
    public function __construct(Proyecto $model)
    {
        parent::__construct($model);
    }

    public function create($title,$url)
    {
        return Proyecto::insertGetId([
            'title'=> $title,
            'url_base'=> $url
        ]);
    }
}
