<?php

namespace App\Repos;

use App\Models\User;

class UserRepo extends RepoBase
{
    public function __construct(User $user)
    {
        parent::__construct($user);
    }

    public function create($data)
    {
        $user = $this->getModel();
        return $user::create($data);
    }
}
