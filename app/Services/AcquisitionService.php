<?php

namespace App\Services;

use App\Exceptions\ItemNotFound;
use App\Exceptions\UnauthorizedException;
use App\Models\Acquisition;
use App\Repos\AcquisitionRepo;
use Illuminate\Support\Facades\DB;
use App\Repos\AcquisitionModuleRepo;

class AcquisitionService
{
    private $acquisitionRepo;
    private $acquisitionModuleRepo;

    public function __construct(AcquisitionRepo $acquisitionRepo, AcquisitionModuleRepo $acquisitionModuleRepo)
    {
        $this->acquisitionRepo = $acquisitionRepo;
        $this->acquisitionModuleRepo = $acquisitionModuleRepo;
    }

    /**
     * Esta funcion devuelve una instancia del modelo Aquisition
     *
     * @return Acquisition
     */
    public function showAllAcquisition()
    {
        return $this->acquisitionRepo->find();
    }

    public function createAcquisition(int $proyect_id, int $cliente_id, array $modules): string
    {
        DB::beginTransaction();
        try {
            $acquisition = $this->acquisitionRepo->create([
                'proyect_id' => $proyect_id,
                'cliente_id' => $cliente_id,
                'is_active' => true
            ]);

            foreach ($modules as $acquisition_module) {
                $this->acquisitionModuleRepo->create([
                    'acquisition_id' => $acquisition->id,
                    'module_id' => $acquisition_module['module_id'],
                    'is_active' => true,
                    'activation_date' => $acquisition_module['activation_date'],
                    'expiration_date' => $acquisition_module['expiration_date'],
                ]);
            }
            DB::commit();

            return "Acquisition created sucessfuly";
        } catch (\Throwable $th) {
            throw new UnauthorizedException('Error');
            DB::rollBack();
        }
    }


    /**
     * Esta funcion actualiza el estatus de la adquisicion
     *
     * @param integer $id
     * @return String
     */
    public function updateStatus(int $id): String
    {
        $acquisition = $this->acquisitionRepo->findById($id);
        if (!$acquisition)
            throw new ItemNotFound('Acquisition not exists');
        $acquisition->is_active = !$acquisition->is_active;
        $acquisition->save();

        return "Aquisition updated";
    }

    public function getOne(int $id)
    {
        return $this->acquisitionRepo->findById($id);
    }

    public function deleteAcquisition(int $id)
    {
        $this->acquisitionRepo->delete($id);
        return "Acquisition deleted successfully";
    }

    public function updateAcquisition(int $id, int $proyect_id, int $cliente_id, array $acquisition_modules)
    {
        DB::beginTransaction();
        try {
            $this->acquisitionRepo->update(
                $id,
                [
                    'proyect_id' => $proyect_id,
                    'cliente_id' => $cliente_id,
                    'is_active' => true
                ]
            );
            foreach ($acquisition_modules as $acquisition_module) {
                if (isset($acquisition_module['id'])) {
                    $this->acquisitionModuleRepo->update([
                        'module_id' => $acquisition_module['module_id'],
                        'is_active' => true,
                        'activation_date' => $acquisition_module['activation_date'],
                        'expiration_date' => $acquisition_module['expiration_date'],
                    ]);
                } else {
                    $this->acquisitionModuleRepo->create([
                        'acquisition_id' => $id,
                        'module_id' => $acquisition_module['module_id'],
                        'is_active' => true,
                        'activation_date' => $acquisition_module['activation_date'],
                        'expiration_date' => $acquisition_module['expiration_date'],
                    ]);
                }
            }
            DB::commit();
            return "Acquisition updated successfully";
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
        }
    }
}
