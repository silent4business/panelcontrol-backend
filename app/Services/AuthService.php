<?php

namespace App\Services;

use App\Exceptions\BadCredentialsException;
use App\Repos\UserRepo;
use Tymon\JWTAuth\JWTAuth;

class  AuthService
{
    private $repo;

    private $auth;

    public function __construct(UserRepo $repo, JWTAuth $auth)
    {
        $this->repo = $repo;
        $this->auth = $auth;
    }

    public function signin($user, $password)
    {
        if (!$token = $this->auth->attempt(['email' => $user, 'password' => $password]))
            throw new BadCredentialsException('Bad credentials');
        return $token;
    }

    public function signout()
    {
        $this->auth->parseToken()->invalidate();

        return "See you :)";
    }

    public function me($email)
    {
        return $this->repo->findFirst(['*'], ['email' => $email]);
    }
}
