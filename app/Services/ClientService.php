<?php

namespace App\Services;

use App\Repos\ClientRepo;

class ClientService
{
    private $clientRepo;

    public function __construct(ClientRepo $clientRepo)
    {
        $this->clientRepo = $clientRepo;
    }

    /**
     * Esta funcion devuelve una instancia de
     * modelo de clientes
     *
     * @return void
     */
    public function showAllClient()
    {
        $client = $this->clientRepo->find();
        return $client;
    }

    /**
     * Esta funcion devuelve un texto
     * cuando se crea un cliente
     *
     * @param [type] $data
     * @return void
     */
    public function createClient($data): string
    {
        $this->clientRepo->create($data);
        return "Client created sucessfuly";
    }

    /**
     * Esta funcion devuelve un texto
     * cuando un cliente se actualiza
     *
     * @param int $id
     * @param array $data
     * @return string
     */
    public function updateClient($id,$data)
    {
        $this->clientRepo->update($id,$data);
        return "Client updated successfully";
    }

    /**
     * Esta funcion devulve una instancia de
     * modelo de cliente cuando el id es
     * igual al que recibe como parametro
     *
     * @param int $id
     * @return Client
     */
    public function findOneClient($id)
    {
        return $this->clientRepo->findById($id);
    }

    /**
     * Esta funcion devuelve un texto
     * cuando se elimina un cliente
     *
     * @param int $id
     * @return string
     */
    public function deleteClient($id): string
    {
        $this->clientRepo->delete($id);
        return "Client deleted successfully";
    }

}
