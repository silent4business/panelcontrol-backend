<?php

namespace App\Services;

use App\Repos\ModuleRepo;
use App\Repos\UserRepo;

class ModuleService
{
    private $moduleRepo;

    private $userRepo;

    public function __construct(ModuleRepo $moduleRepo, UserRepo $userRepo)
    {
        $this->moduleRepo = $moduleRepo;

        $this->userRepo = $userRepo;
    }

    public function getAll()
    {
        return $this->moduleRepo->find();
    }

    public function buyModule($project_id, $module_id): String
    {
        return true;
    }

    /**
     * Esta funcion devuelve un texto
     * cuando se elimina un Proyecto
     *
     * @param int $id
     * @return string
     */
    public function deleteModule(int $id): string
    {
        $this->moduleRepo->delete($id);
        return "Module deleted successfully";
    }
}
