<?php

namespace App\Services;

use App\Exceptions\ItemNotFound;
use App\Exceptions\PaymentRequiredException;
use App\Models\Proyecto;
use App\Repos\ModuleRepo;
use App\Repos\ProyectRepo;
use App\Repos\UsersClientsRepo;

class ProyectService
{
    private $proyectRepo;

    private $usersClientsRepo;

    private $moduleRepo;

    public function __construct(ProyectRepo $proyectRepo, UsersClientsRepo $usersClientsRepo, ModuleRepo $moduleRepo)
    {
        $this->proyectRepo = $proyectRepo;
        $this->usersClientsRepo = $usersClientsRepo;
        $this->moduleRepo =$moduleRepo;
    }

    /**
     * Esta funcion devuelve una instancia de
     * modelo de proyecto
     *
     * @param array $filters
     * @return void
     */
    public function getAll($filters = [])
    {
        return $this->proyectRepo->find(['*'], $filters);
    }

    /**
     * Esta funcion devulve una instancia de
     * modelo de Proyecto cuando el id es
     * igual al que recibe como parametro
     *
     * @param int $id
     * @return Proyecto
     */
    public function findById(int $id): Proyecto
    {
        return $this->proyectRepo->findById($id);
    }

    public function validateModules($project_id, $module_id)
    {
        if(!$proyect = $this->proyectRepo->findById($project_id))
            throw new ItemNotFound('Proyect not exists');

        if(!$this->moduleRepo->findById($module_id))
            throw new ItemNotFound('Module not exists');

        $module = [];
        foreach($proyect->acquisitions as $acquisition){
            if($acquisition->module_id == $module_id)
                $module = $acquisition;
        }
        if(empty($module))
            throw new PaymentRequiredException('Necesitas comprar este modulo para tener acceso');

        if(!$module->is_active)
            throw new PaymentRequiredException('Necesitas renovar el pago del modulo para acceder a sus funcionalidades');

        return [
            'options' => [
                'test' => '1234'
            ]
        ];
    }

    /**
     * Esta funcion devuelve un texto
     * cuando se crea un proyecto
     *
     * @param array $modules
     * @param string $title
     * @param string $url
     * @return string
     */
    public function createProyect(array $modules, string $title,string $url): string
    {
        $proyecto = $this->proyectRepo->create($title,$url);

        foreach($modules as $module)
        {
            $this->moduleRepo->create([
                'title'=>$module['title'],
                'proyecto_id'=>$proyecto,
                'url'=>$module['url']
            ]);
        }
        return "Project created sucessfuly";
    }

    /**
     * Esta funcion devuelve un texto
     * cuando se actualiza un proyecto
     *
     * @param array $modules
     * @param string $title
     * @param string $url
     * @param int $id
     * @return string
     */
    public function updateProyect($modules,string $title,string $url,int $id): string
    {
        $this->proyectRepo->update($id,
        [
            'title'=>$title,
            'title'=>$url
        ]);
        foreach($modules as $module)
        {
            $this->moduleRepo->update(
                $module['id'],
                [
                    'title'=>$module['title'],
                    'url'=>$module['url']
            ]);
        }
        return "Project updated sucessfuly";


    }

    /**
     * Esta funcion devuelve un texto
     * cuando se elimina un Proyecto
     *
     * @param int $id
     * @return string
     */
    public function deleteProyect(int $id): string
    {
        $this->proyectRepo->delete($id);
        return "Project deleted successfully";
    }

}
