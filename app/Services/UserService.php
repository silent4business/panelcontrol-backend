<?php

namespace App\Services;

use App\Repos\UserRepo;

class UserService
{
    private $repo;

    public function __construct(UserRepo $repo)
    {
        $this->repo = $repo;
    }

    public function createUser($data): string
    {
        $user = $this->repo->create($data);
        return "User created sucessfully";
    }

    /**
     * Esta funcion devuelve una instancia de
     * modelo de usuarios cuando el id es
     * igual al que recibe como parametro
     *
     * @param int $id
     * @return User
     */
    public function findOneUSer($id)
    {
        return $this->repo->findById($id);
    }
    /**
     * Esta funcion devuelve una instancia de
     * modelo de usuarios
     * @return User
     */
    public function showAllUser()
    {
        return $this->repo->find();
    }

    /**
     * Esta funcion devulve un texto de
     * usuarios cuando un usuario se
     * actualiza
     *
     * @param int $id
     * @param array $data
     * @return string
     */
    public function updateUser($id,$data): string
    {
        $this->repo->update($id,$data);
        return "User updated successfully";
    }

    /**
     * Esta funcion devuelve un texto
     * correspondiente a eliminar un usuario
     *
     * @param int $id
     * @return string
     */
    public function deleteUser($id): string
    {
        $this->repo->delete($id);
        return "User deleted successfully";
    }
}
