<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Brackets\AdminAuth\Models\AdminUser::class, function (Faker\Generator $faker) {
    return [
        'activated' => true,
        'created_at' => $faker->dateTime,
        'deleted_at' => null,
        'email' => $faker->email,
        'first_name' => $faker->firstName,
        'forbidden' => $faker->boolean(),
        'language' => 'en',
        'last_login_at' => $faker->dateTime,
        'last_name' => $faker->lastName,
        'password' => bcrypt($faker->password),
        'remember_token' => null,
        'updated_at' => $faker->dateTime,
        
    ];
});/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Subscription::class, static function (Faker\Generator $faker) {
    return [
        
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Product::class, static function (Faker\Generator $faker) {
    return [
        'active' => $faker->boolean(),
        'created' => $faker->dateTime,
        'created_at' => $faker->dateTime,
        'default_price' => $faker->sentence,
        'description' => $faker->sentence,
        'livemode' => $faker->boolean(),
        'name' => $faker->firstName,
        'object' => $faker->sentence,
        'product_id' => $faker->sentence,
        'shippable' => $faker->boolean(),
        'statement_descriptor' => $faker->sentence,
        'tax_code' => $faker->sentence,
        'unit_label' => $faker->sentence,
        'updated' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'url' => $faker->sentence,
        
        'features' => ['en' => $faker->sentence],
        'images' => ['en' => $faker->sentence],
        'metadata' => ['en' => $faker->sentence],
        'package_dimensions' => ['en' => $faker->sentence],
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Coupon::class, static function (Faker\Generator $faker) {
    return [
        'amount_off' => $faker->randomNumber(5),
        'coupons_id' => $faker->sentence,
        'created' => $faker->sentence,
        'currency' => $faker->sentence,
        'duration' => $faker->sentence,
        'duration_in_months' => $faker->randomNumber(5),
        'livemode' => $faker->boolean(),
        'max_redemptions' => $faker->randomNumber(5),
        'name' => $faker->firstName,
        'object' => $faker->sentence,
        'percent_off' => $faker->randomFloat,
        'redeem_by' => $faker->sentence,
        'times_redeemed' => $faker->randomNumber(5),
        'valid' => $faker->boolean(),
        
        'metadata' => ['en' => $faker->sentence],
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Promotion::class, static function (Faker\Generator $faker) {
    return [
        'active' => $faker->boolean(),
        'code' => $faker->sentence,
        'coupon_id' => $faker->sentence,
        'created' => $faker->dateTime,
        'created_at' => $faker->dateTime,
        'customer' => $faker->sentence,
        'expires_at' => $faker->dateTime,
        'livemode' => $faker->boolean(),
        'max_redemptions' => $faker->randomNumber(5),
        'object' => $faker->sentence,
        'promotion_id' => $faker->sentence,
        'times_redeemed' => $faker->randomNumber(5),
        'updated_at' => $faker->dateTime,
        
        'metadata' => ['en' => $faker->sentence],
        'restrictions' => ['en' => $faker->sentence],
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Dashboard::class, static function (Faker\Generator $faker) {
    return [
        'subscription_id' => $faker->sentence,
        'nombre' => $faker->sentence,
        'servicio' => $faker->sentence,
        'status' => $faker->sentence,
        'currency' => $faker->sentence,
        'object' => $faker->sentence,
        'interval' => $faker->sentence,
        'interval_count' => $faker->sentence,
        'current_period_end' => $faker->sentence,
        'current_period_start' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
