<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->string('razon_social');
            $table->string('contacto');
            $table->string('domicilio');
            $table->string('representante')->nullable();//es quien se contacta con la organizacion
            $table->string('representante_email')->nullable();
            $table->string('representante_telefono')->nullable();
            $table->string('consultor')->nullable();//es quien lleva la adquision del servicio
            $table->string('consultor_email')->nullable();
            $table->string('consultor_telefono')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('clientes');
    }
};
