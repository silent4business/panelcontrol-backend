<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('acquisitions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('module_id')->references('id')->on('modulos')->onDelete('cascade');
            $table->foreignId('proyect_id')->references('id')->on('proyectos')->onDelete('cascade');
            $table->foreignId('cliente_id')->references('id')->on('clientes')->onDelete('cascade');
            $table->date('activation_date');
            $table->date('expiration_date');
            $table->boolean('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('acquisition');
    }
};
