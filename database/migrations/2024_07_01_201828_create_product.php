<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string('product_id');
            $table->string('object');
            $table->boolean('active');
            $table->timestamp('created')->nullable();
            $table->string('default_price')->nullable();
            $table->string('description')->nullable();
            $table->json('images');
            $table->json('features');
            $table->boolean('livemode');
            $table->json('metadata')->nullable();
            $table->string('name');
            $table->json('package_dimensions')->nullable();
            $table->boolean('shippable')->nullable();
            $table->string('statement_descriptor')->nullable();
            $table->string('tax_code')->nullable();
            $table->string('unit_label')->nullable();
            $table->timestamp('updated')->nullable();
            $table->string('url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product');
    }
};
