<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->string('coupons_id');
            $table->string('object');
            $table->integer('amount_off')->nullable();
            $table->bigInteger('created');
            $table->string('currency')->nullable();
            $table->string('duration');
            $table->integer('duration_in_months')->nullable();
            $table->boolean('livemode')->default(false);
            $table->integer('max_redemptions')->nullable();
            $table->json('metadata')->nullable();
            $table->string('name')->nullable();
            $table->float('percent_off')->nullable();
            $table->bigInteger('redeem_by')->nullable();
            $table->integer('times_redeemed')->default(0);
            $table->boolean('valid')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('coupons');
    }
};
