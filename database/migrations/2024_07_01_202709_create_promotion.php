<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('promotion', function (Blueprint $table) {
            $table->id();
            $table->string('promotion_id');
            $table->string('object');
            $table->boolean('active');
            $table->string('code');
            $table->string('coupon_id');
            $table->timestamp('created')->nullable();
            $table->string('customer')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->boolean('livemode');
            $table->integer('max_redemptions')->nullable();
            $table->json('metadata');
            $table->json('restrictions');
            $table->integer('times_redeemed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('promotion');
    }
};
