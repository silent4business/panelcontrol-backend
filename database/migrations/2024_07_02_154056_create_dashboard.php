<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dashboard', function (Blueprint $table) {
            $table->id();
            $table->string('subscription_id');
            $table->string('nombre');
            $table->string('servicio');
            $table->string('status');
            $table->string('currency');
            $table->string('object');
            $table->string('interval');
            $table->string('interval_count');
            $table->string('current_period_end');
            $table->string('current_period_start');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('dashboard');
    }
};
