<?php

namespace Database\Seeders;

use App\Models\Acquisition;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AcquisitionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Acquisition::create([
            'module_id' => 1,
            'proyect_id' => 1,
            'cliente_id' => 1,
            'activation_date' => Carbon::create(2023, 6, 1),
            'expiration_date' => Carbon::create(2023, 10, 10),
            'is_active' => true
        ]);

        Acquisition::create([
            'module_id' => 2,
            'proyect_id' => 1,
            'cliente_id' => 1,
            'activation_date' => Carbon::create(2023, 4, 1),
            'expiration_date' => Carbon::create(2023, 5, 10),
            'is_active' => false
        ]);
    }
}
