<?php

namespace Database\Seeders;

use App\Models\Cliente;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Cliente::create([
            'name' => 'Cliente',
            'domicilio' => 'Direccion del cliente',
            'razon_social' => 'Razon social del cliente',
            'contacto' => 'Contacto del cliente',
            'email' => 'cliente@mail.com',
            'representante' => 'Representante del cliente',
            'representante_email' => 'representante@mail.com',
            'representante_telefono' => '555555555',
            'consultor' => 'Consultor',
            'consultor_email' => 'consultor@mail.com',
            'consultor_telefono' => '555555555',
        ]);
    }
}
