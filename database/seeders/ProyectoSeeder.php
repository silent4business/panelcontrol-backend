<?php

namespace Database\Seeders;

use App\Models\Modulo;
use App\Models\Proyecto;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProyectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Proyecto::create(['title'=>'Elearning', 'url_base'=>"https://nuup.silent4business.com"]);
        Modulo::create(['title'=>'Timesheet','proyecto_id'=>1,'url'=>'/courses']);
    }
}
