import AppForm from '../app-components/Form/AppForm';

Vue.component('coupon-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                amount_off:  '' ,
                coupons_id:  '' ,
                created:  '' ,
                currency:  '' ,
                duration:  '' ,
                duration_in_months:  '' ,
                livemode:  false ,
                max_redemptions:  '' ,
                metadata:  this.getLocalizedFormDefaults() ,
                name:  '' ,
                object:  '' ,
                percent_off:  '' ,
                redeem_by:  '' ,
                times_redeemed:  '' ,
                valid:  false ,
            },
            showMetadata: false // Variable de control de visibilidad
        }
    },
    methods: {
        toggleMetadataVisibility() {
            this.showMetadata = !this.showMetadata;
        }
    }

});
