import AppForm from '../app-components/Form/AppForm';

Vue.component('dashboard-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                subscription_id:  '' ,
                nombre:  '' ,
                servicio:  '' ,
                status:  '' ,
                currency:  '' ,
                object:  '' ,
                interval:  '' ,
                interval_count:  '' ,
                current_period_end:  '' ,
                current_period_start:  '' ,
                
            }
        }
    }

});