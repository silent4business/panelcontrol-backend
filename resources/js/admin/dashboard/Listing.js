import AppListing from '../app-components/Listing/AppListing';

Vue.component('dashboard-listing', {
    mixins: [AppListing]
});