import AppForm from '../app-components/Form/AppForm';


Vue.component('product-form', {
    mixins: [AppForm],
    data() {
        return {
            price: '',
            selectedCurrency: 'USD', // Valor inicial del select
            form: {
                active: false,
                created: '',
                default_price: '',
                description: '',
                features: this.getLocalizedFormDefaults(),
                images: this.getLocalizedFormDefaults(),
                livemode: false,
                metadata: this.getLocalizedFormDefaults(),
                name: '',
                object: '',
                package_dimensions: this.getLocalizedFormDefaults(),
                product_id: '',
                shippable: false,
                statement_descriptor: '',
                tax_code: '',
                unit_label: '',
                updated: '',
                url: ''
            },
            showDatas: false,
            isFormLocalized: false
        };
    },
    computed: {
        formattedDefaultPrice: {
            get() {
                return this.price ? `${parseFloat(this.price).toFixed(2)}` : '';
            },
            set(value) {
                // Remove non-numeric characters except dot (.) and minus (-)
                const numericValue = value.replace(/[^\d.-]/g, '');
                this.price = numericValue;

                // Update form field with cleaned numeric value
                this.form.default_price = numericValue;
            }
        }
    },
    methods: {
        validateInput() {
            // Remove non-numeric characters except dot (.) and minus (-)
            this.form.default_price = this.form.default_price.replace(/[^\d.-]/g, '');
        },
        updateCurrency(event) {
            // Aquí puedes manejar la lógica cuando se cambie la divisa seleccionada
            console.log('Currency changed to:', this.selectedCurrency);
        },
        // Aquí definirías más métodos según tus necesidades
    }
});
