import AppForm from '../app-components/Form/AppForm';

Vue.component('promotion-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                active:  false ,
                code:  '' ,
                coupon_id:  '' ,
                created:  '' ,
                customer:  '' ,
                expires_at:  '' ,
                livemode:  false ,
                max_redemptions:  '' ,
                metadata:  this.getLocalizedFormDefaults() ,
                object:  '' ,
                promotion_id:  '' ,
                restrictions:  this.getLocalizedFormDefaults() ,
                times_redeemed:  '' ,
                
            }
        }
    }

});