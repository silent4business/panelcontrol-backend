import AppListing from '../app-components/Listing/AppListing';

Vue.component('subscription-listing', {
    mixins: [AppListing]
});