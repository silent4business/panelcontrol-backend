<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'last_login_at' => 'Last login',
            'activated' => 'Activated',
            'email' => 'Email',
            'first_name' => 'First name',
            'forbidden' => 'Forbidden',
            'language' => 'Language',
            'last_name' => 'Last name',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
                
            //Belongs to many relations
            'roles' => 'Roles',
                
        ],
    ],

    'subscription' => [
        'title' => 'Subscription',

        'actions' => [
            'index' => 'Subscription',
            'create' => 'New Subscription',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            
        ],
    ],

    'product' => [
        'title' => 'Product',

        'actions' => [
            'index' => 'Product',
            'create' => 'New Product',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'active' => 'Active',
            'created' => 'Created',
            'default_price' => 'Default price',
            'description' => 'Description',
            'features' => 'Features',
            'images' => 'Images',
            'livemode' => 'Livemode',
            'metadata' => 'Metadata',
            'name' => 'Name',
            'object' => 'Object',
            'package_dimensions' => 'Package dimensions',
            'product_id' => 'Product',
            'shippable' => 'Shippable',
            'statement_descriptor' => 'Statement descriptor',
            'tax_code' => 'Tax code',
            'unit_label' => 'Unit label',
            'updated' => 'Updated',
            'url' => 'Url',
            
        ],
    ],

    'coupon' => [
        'title' => 'Coupons',

        'actions' => [
            'index' => 'Coupons',
            'create' => 'New Coupon',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'amount_off' => 'Amount off',
            'coupons_id' => 'Coupons',
            'created' => 'Created',
            'currency' => 'Currency',
            'duration' => 'Duration',
            'duration_in_months' => 'Duration in months',
            'livemode' => 'Livemode',
            'max_redemptions' => 'Max redemptions',
            'metadata' => 'Metadata',
            'name' => 'Name',
            'object' => 'Object',
            'percent_off' => 'Percent off',
            'redeem_by' => 'Redeem by',
            'times_redeemed' => 'Times redeemed',
            'valid' => 'Valid',
            
        ],
    ],

    'promotion' => [
        'title' => 'Promotion',

        'actions' => [
            'index' => 'Promotion',
            'create' => 'New Promotion',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'active' => 'Active',
            'code' => 'Code',
            'coupon_id' => 'Coupon',
            'created' => 'Created',
            'customer' => 'Customer',
            'expires_at' => 'Expires at',
            'livemode' => 'Livemode',
            'max_redemptions' => 'Max redemptions',
            'metadata' => 'Metadata',
            'object' => 'Object',
            'promotion_id' => 'Promotion',
            'restrictions' => 'Restrictions',
            'times_redeemed' => 'Times redeemed',
            
        ],
    ],

    'dashboard' => [
        'title' => 'Dashboard',

        'actions' => [
            'index' => 'Dashboard',
            'create' => 'New Dashboard',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'subscription_id' => 'Subscription',
            'nombre' => 'Nombre',
            'servicio' => 'Servicio',
            'status' => 'Status',
            'currency' => 'Currency',
            'object' => 'Object',
            'interval' => 'Interval',
            'interval_count' => 'Interval count',
            'current_period_end' => 'Current period end',
            'current_period_start' => 'Current period start',
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];