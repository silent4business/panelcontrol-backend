<div class="row form-inline" style="padding-bottom: 10px;" v-cloak>
    <div
        :class="{
            'col-xl-10 col-md-11 text-right': !
                isFormLocalized,
            'col text-center': isFormLocalized,
            'hidden': onSmallScreen
        }">
        <small>{{ trans('brackets/admin-ui::admin.forms.currently_editing_translation') }}<span
                v-if="!isFormLocalized && otherLocales.length > 1">
                {{ trans('brackets/admin-ui::admin.forms.more_can_be_managed') }}</span><span v-if="!isFormLocalized"> |
                <a href="#"
                    @click.prevent="showLocalization">{{ trans('brackets/admin-ui::admin.forms.manage_translations') }}</a></span></small>
        <i class="localization-error" v-if="!isFormLocalized && showLocalizedValidationError"></i>
    </div>

    <div class="col text-center"
        :class="{ 'language-mobile': onSmallScreen, 'has-error': !isFormLocalized && showLocalizedValidationError }"
        v-if="isFormLocalized || onSmallScreen" v-cloak>
        <small>{{ trans('brackets/admin-ui::admin.forms.choose_translation_to_edit') }}
            <select class="form-control" v-model="currentLocale">
                <option :value="defaultLocale" v-if="onSmallScreen">@{{ defaultLocale.toUpperCase() }}</option>
                <option v-for="locale in otherLocales" :value="locale">@{{ locale.toUpperCase() }}</option>
            </select>
            <i class="localization-error" v-if="isFormLocalized && showLocalizedValidationError"></i>
            <span>|</span>
            <a href="#" @click.prevent="hideLocalization">{{ trans('brackets/admin-ui::admin.forms.hide') }}</a>
        </small>
    </div>
</div>


<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('duration'), 'has-success': fields.duration && fields.duration.valid }">
    <label for="duration" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.duration') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select v-model="form.duration" v-validate="'required'" @input="validate($event)" class="form-control"
            :class="{
                'form-control-danger': errors.has('duration'),
                'form-control-success': fields.duration && fields.duration.valid
            }"
            id="duration" name="duration">
            <option value="">Seleccione...</option>
            <option value="once">Una vez</option>
            <option value="repeating">Varios Meses</option>
            <option value="forever">Para siempre</option>
        </select>
        <div v-if="errors.has('duration')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('duration') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
    :class="{
        'has-danger': errors.has('duration_in_months'),
        'has-success': fields.duration_in_months && fields
            .duration_in_months.valid
    }">
    <label for="duration_in_months" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.duration_in_months') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.duration_in_months" v-validate="'integer'" @input="validate($event)"
            class="form-control"
            :class="{
                'form-control-danger': errors.has('duration_in_months'),
                'form-control-success': fields
                    .duration_in_months && fields.duration_in_months.valid
            }"
            id="duration_in_months" name="duration_in_months"
            placeholder="{{ trans('admin.coupon.columns.duration_in_months') }}">
        <div v-if="errors.has('duration_in_months')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('duration_in_months') }}</div>
    </div>
</div>


<div class="form-group row align-items-center"
    :class="{
        'has-danger': errors.has('max_redemptions'),
        'has-success': fields.max_redemptions && fields.max_redemptions
            .valid
    }">
    <label for="max_redemptions" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.max_redemptions') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.max_redemptions" v-validate="'integer'" @input="validate($event)"
            class="form-control"
            :class="{
                'form-control-danger': errors.has('max_redemptions'),
                'form-control-success': fields.max_redemptions &&
                    fields.max_redemptions.valid
            }"
            id="max_redemptions" name="max_redemptions"
            placeholder="{{ trans('admin.coupon.columns.max_redemptions') }}">
        <div v-if="errors.has('max_redemptions')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('max_redemptions') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
    <label for="name" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.name') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="''" @input="validate($event)" class="form-control"
            :class="{ 'form-control-danger': errors.has('name'), 'form-control-success': fields.name && fields.name.valid }"
            id="name" name="name" placeholder="{{ trans('admin.coupon.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>


<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('percent_off'), 'has-success': fields.percent_off && fields.percent_off.valid }">
    <label for="percent_off" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.percent_off') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.percent_off" v-validate="'decimal'" @input="validate($event)"
            class="form-control"
            :class="{
                'form-control-danger': errors.has('percent_off'),
                'form-control-success': fields.percent_off && fields
                    .percent_off.valid
            }"
            id="percent_off" name="percent_off" placeholder="{{ trans('admin.coupon.columns.percent_off') }}">
        <div v-if="errors.has('percent_off')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('percent_off') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('redeem_by'), 'has-success': fields.redeem_by && fields.redeem_by.valid }">
    <label for="redeem_by" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.redeem_by') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.redeem_by" v-validate="''" @input="validate($event)"
            class="form-control"
            :class="{
                'form-control-danger': errors.has('redeem_by'),
                'form-control-success': fields.redeem_by && fields
                    .redeem_by.valid
            }"
            id="redeem_by" name="redeem_by" placeholder="{{ trans('admin.coupon.columns.redeem_by') }}">
        <div v-if="errors.has('redeem_by')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('redeem_by') }}
        </div>
    </div>
</div>
<div v-if="showMetadata">
    <div class="form-group row align-items-center"
        :class="{ 'has-danger': errors.has('object'), 'has-success': fields.object && fields.object.valid }">
        <label for="object" class="col-form-label text-md-right"
            :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.object') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.object" v-validate="''" @input="validate($event)"
                class="form-control"
                :class="{
                    'form-control-danger': errors.has('object'),
                    'form-control-success': fields.object && fields.object
                        .valid
                }"
                id="object" name="object" placeholder="{{ trans('admin.coupon.columns.object') }}">
            <div v-if="errors.has('object')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('object') }}
            </div>
        </div>
    </div>
</div>
<div v-if="showMetadata">
    <div class="form-check row"
        :class="{ 'has-danger': errors.has('livemode'), 'has-success': fields.livemode && fields.livemode.valid }">
        <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
            <input class="form-check-input" id="livemode" type="checkbox" v-model="form.livemode" v-validate="''"
                data-vv-name="livemode" name="livemode_fake_element">
            <label class="form-check-label" for="livemode">
                {{ trans('admin.coupon.columns.livemode') }}
            </label>
            <input type="hidden" name="livemode" :value="form.livemode">
            <div v-if="errors.has('livemode')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('livemode') }}
            </div>
        </div>
    </div>
</div>
<div v-if="showMetadata">
    <div class="form-group row align-items-center"
        :class="{ 'has-danger': errors.has('times_redeemed'), 'has-success': fields.times_redeemed && fields.times_redeemed
                .valid }">
        <label for="times_redeemed" class="col-form-label text-md-right"
            :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.times_redeemed') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.times_redeemed" v-validate="'required|integer'"
                @input="validate($event)" class="form-control"
                :class="{
                    'form-control-danger': errors.has('times_redeemed'),
                    'form-control-success': fields.times_redeemed &&
                        fields.times_redeemed.valid
                }"
                id="times_redeemed" name="times_redeemed"
                placeholder="{{ trans('admin.coupon.columns.times_redeemed') }}">
            <div v-if="errors.has('times_redeemed')" class="form-control-feedback form-text" v-cloak>
                @{{ errors.first('times_redeemed') }}</div>
        </div>
    </div>
</div>
<div v-if="showMetadata">
    <div class="form-check row"
        :class="{ 'has-danger': errors.has('valid'), 'has-success': fields.valid && fields.valid.valid }">
        <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
            <input class="form-check-input" id="valid" type="checkbox" v-model="form.valid" v-validate="''"
                data-vv-name="valid" name="valid_fake_element">
            <label class="form-check-label" for="valid">
                {{ trans('admin.coupon.columns.valid') }}
            </label>
            <input type="hidden" name="valid" :value="form.valid">
            <div v-if="errors.has('valid')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('valid') }}
            </div>
        </div>
    </div>
</div>
<div v-if="showMetadata">
    <div class="row">
        @foreach ($locales as $locale)
            <div class="col-md" v-show="shouldShowLangGroup('{{ $locale }}')" v-cloak>
                <div class="form-group row align-items-center"
                    :class="{
                        'has-danger': errors.has('metadata_{{ $locale }}'),
                        'has-success': fields
                            .metadata_{{ $locale }} && fields.metadata_{{ $locale }}.valid
                    }">
                    <label for="metadata_{{ $locale }}"
                        class="col-md-2 col-form-label text-md-right">{{ trans('admin.coupon.columns.metadata') }}</label>
                    <div class="col-md-9" :class="{ 'col-xl-8': !isFormLocalized }">
                        <input type="text" v-model="form.metadata.{{ $locale }}" v-validate="''"
                            @input="validate($event)" class="form-control"
                            :class="{
                                'form-control-danger': errors.has(
                                    'metadata_{{ $locale }}'),
                                'form-control-success': fields
                                    .metadata_{{ $locale }} && fields.metadata_{{ $locale }}.valid
                            }"
                            id="metadata_{{ $locale }}" name="metadata_{{ $locale }}"
                            placeholder="{{ trans('admin.coupon.columns.metadata') }}">
                        <div v-if="errors.has('metadata_{{ $locale }}')" class="form-control-feedback form-text"
                            v-cloak>{{ '{{' }} errors.first('metadata_{{ $locale }}') }}</div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
<div v-if="showMetadata">
    <div class="form-group row align-items-center"
        :class="{ 'has-danger': errors.has('amount_off'), 'has-success': fields.amount_off && fields.amount_off.valid }">
        <label for="amount_off" class="col-form-label text-md-right"
            :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.amount_off') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.amount_off" v-validate="'integer'" @input="validate($event)"
                class="form-control"
                :class="{
                    'form-control-danger': errors.has('amount_off'),
                    'form-control-success': fields.amount_off && fields
                        .amount_off.valid
                }"
                id="amount_off" name="amount_off" placeholder="{{ trans('admin.coupon.columns.amount_off') }}">
            <div v-if="errors.has('amount_off')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('amount_off') }}
            </div>
        </div>
    </div>
</div>
<div v-if="showMetadata">
    <div class="form-group row align-items-center"
        :class="{ 'has-danger': errors.has('coupons_id'), 'has-success': fields.coupons_id && fields.coupons_id.valid }">
        <label for="coupons_id" class="col-form-label text-md-right"
            :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.coupons_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.coupons_id" v-validate="''" @input="validate($event)"
                class="form-control"
                :class="{
                    'form-control-danger': errors.has('coupons_id'),
                    'form-control-success': fields.coupons_id && fields
                        .coupons_id.valid
                }"
                id="coupons_id" name="coupons_id" placeholder="{{ trans('admin.coupon.columns.coupons_id') }}">
            <div v-if="errors.has('coupons_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('coupons_id') }}
            </div>
        </div>
    </div>
</div>
<div v-if="showMetadata">
    <div class="form-group row align-items-center"
        :class="{ 'has-danger': errors.has('created'), 'has-success': fields.created && fields.created.valid }">
        <label for="created" class="col-form-label text-md-right"
            :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.created') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.created" v-validate="''" @input="validate($event)" class="form-control"
                :class="{
                    'form-control-danger': errors.has('created'),
                    'form-control-success': fields.created && fields.created
                        .valid
                }"
                id="created" name="created" placeholder="{{ trans('admin.coupon.columns.created') }}">
            <div v-if="errors.has('created')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('created') }}
            </div>
        </div>
    </div>
</div>
<div v-if="showMetadata">
    <div class="form-group row align-items-center"
        :class="{ 'has-danger': errors.has('currency'), 'has-success': fields.currency && fields.currency.valid }">
        <label for="currency" class="col-form-label text-md-right"
            :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.currency') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.currency" v-validate="''" @input="validate($event)" class="form-control"
                :class="{
                    'form-control-danger': errors.has('currency'),
                    'form-control-success': fields.currency && fields.currency
                        .valid
                }"
                id="currency" name="currency" placeholder="{{ trans('admin.coupon.columns.currency') }}">
            <div v-if="errors.has('currency')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('currency') }}
            </div>
        </div>
    </div>
</div>
