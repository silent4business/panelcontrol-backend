@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.coupon.actions.index'))

@section('body')

    <coupon-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/coupons') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.coupon.actions.index') }}
                        <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/coupons/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.coupon.actions.create') }}</a>
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">

                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th class="bulk-checkbox">
                                            <input class="form-check-input" id="enabled" type="checkbox" v-model="isClickedAll" v-validate="''" data-vv-name="enabled"  name="enabled_fake_element" @click="onBulkItemsClickedAllWithPagination()">
                                            <label class="form-check-label" for="enabled">
                                                #
                                            </label>
                                        </th>
                                        <th is='sortable' :column="'id'">{{ trans('admin.coupon.columns.id') }}</th>
                                        <th is='sortable' :column="'amount_off'">{{ trans('admin.coupon.columns.amount_off') }}</th>
                                        <th is='sortable' :column="'coupons_id'">{{ trans('admin.coupon.columns.coupons_id') }}</th>
                                        <th is='sortable' :column="'created'">{{ trans('admin.coupon.columns.created') }}</th>
                                        <th is='sortable' :column="'currency'">{{ trans('admin.coupon.columns.currency') }}</th>
                                        <th is='sortable' :column="'duration'">{{ trans('admin.coupon.columns.duration') }}</th>
                                        <th is='sortable' :column="'duration_in_months'">{{ trans('admin.coupon.columns.duration_in_months') }}</th>

                                        <th is='sortable' :column="'livemode'">{{ trans('admin.coupon.columns.livemode') }}</th>
                                        <th is='sortable' :column="'max_redemptions'">{{ trans('admin.coupon.columns.max_redemptions') }}</th>
                                        {{-- <th is='sortable' :column="'metadata'">{{ trans('admin.coupon.columns.metadata') }}</th> --}}
                                        <th is='sortable' :column="'name'">{{ trans('admin.coupon.columns.name') }}</th>
                                        <th is='sortable' :column="'object'">{{ trans('admin.coupon.columns.object') }}</th>
                                        <th is='sortable' :column="'percent_off'">{{ trans('admin.coupon.columns.percent_off') }}</th>
                                        <th is='sortable' :column="'redeem_by'">{{ trans('admin.coupon.columns.redeem_by') }}</th>
                                        <th is='sortable' :column="'times_redeemed'">{{ trans('admin.coupon.columns.times_redeemed') }}</th>
                                        <th is='sortable' :column="'valid'">{{ trans('admin.coupon.columns.valid') }}</th>

                                        <th></th>
                                    </tr>
                                    <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                        <td class="bg-bulk-info d-table-cell text-center" colspan="18">
                                            <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a href="#" class="text-primary" @click="onBulkItemsClickedAll('/admin/coupons')" v-if="(clickedBulkItemsCount < pagination.state.total)"> <i class="fa" :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                        href="#" class="text-primary" @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>

                                            <span class="pull-right pr-2">
                                                <button class="btn btn-sm btn-danger pr-3 pl-3" @click="bulkDelete('/admin/coupons/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                            </span>

                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        <td class="bulk-checkbox">
                                            <input class="form-check-input" :id="'enabled' + item.id" type="checkbox" v-model="bulkItems[item.id]" v-validate="''" :data-vv-name="'enabled' + item.id"  :name="'enabled' + item.id + '_fake_element'" @click="onBulkItemClicked(item.id)" :disabled="bulkCheckingAllLoader">
                                            <label class="form-check-label" :for="'enabled' + item.id">
                                            </label>
                                        </td>
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ item.amount_off }}</td>
                                        <td>@{{ item.coupons_id }}</td>
                                        <td>@{{ item.created }}</td>
                                        <td>@{{ item.currency }}</td>
                                        <td>@{{ item.duration }}</td>
                                        <td>@{{ item.duration_in_months }}</td>
                                        <td>@{{ item.livemode }}</td>
                                        <td>@{{ item.max_redemptions }}</td>
                                        {{-- <td>@{{ item.metadata }}</td> --}}
                                        <td>@{{ item.name }}</td>
                                        <td>@{{ item.object }}</td>
                                        <td>@{{ item.percent_off }}</td>
                                        <td>@{{ item.redeem_by }}</td>
                                        <td>@{{ item.times_redeemed }}</td>
                                        <td>@{{ item.valid }}</td>

                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                </div>
                                                <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                    <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                <a class="btn btn-primary btn-spinner" href="{{ url('admin/coupons/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.coupon.actions.create') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </coupon-listing>

@endsection
