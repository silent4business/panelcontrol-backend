<div class="form-group row align-items-center" :class="{'has-danger': errors.has('subscription_id'), 'has-success': fields.subscription_id && fields.subscription_id.valid }">
    <label for="subscription_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.dashboard.columns.subscription_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.subscription_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('subscription_id'), 'form-control-success': fields.subscription_id && fields.subscription_id.valid}" id="subscription_id" name="subscription_id" placeholder="{{ trans('admin.dashboard.columns.subscription_id') }}">
        <div v-if="errors.has('subscription_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('subscription_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nombre'), 'has-success': fields.nombre && fields.nombre.valid }">
    <label for="nombre" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.dashboard.columns.nombre') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nombre" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nombre'), 'form-control-success': fields.nombre && fields.nombre.valid}" id="nombre" name="nombre" placeholder="{{ trans('admin.dashboard.columns.nombre') }}">
        <div v-if="errors.has('nombre')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nombre') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('servicio'), 'has-success': fields.servicio && fields.servicio.valid }">
    <label for="servicio" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.dashboard.columns.servicio') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.servicio" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('servicio'), 'form-control-success': fields.servicio && fields.servicio.valid}" id="servicio" name="servicio" placeholder="{{ trans('admin.dashboard.columns.servicio') }}">
        <div v-if="errors.has('servicio')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('servicio') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('status'), 'has-success': fields.status && fields.status.valid }">
    <label for="status" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.dashboard.columns.status') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.status" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('status'), 'form-control-success': fields.status && fields.status.valid}" id="status" name="status" placeholder="{{ trans('admin.dashboard.columns.status') }}">
        <div v-if="errors.has('status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('status') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('currency'), 'has-success': fields.currency && fields.currency.valid }">
    <label for="currency" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.dashboard.columns.currency') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.currency" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('currency'), 'form-control-success': fields.currency && fields.currency.valid}" id="currency" name="currency" placeholder="{{ trans('admin.dashboard.columns.currency') }}">
        <div v-if="errors.has('currency')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('currency') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('object'), 'has-success': fields.object && fields.object.valid }">
    <label for="object" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.dashboard.columns.object') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.object" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('object'), 'form-control-success': fields.object && fields.object.valid}" id="object" name="object" placeholder="{{ trans('admin.dashboard.columns.object') }}">
        <div v-if="errors.has('object')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('object') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('interval'), 'has-success': fields.interval && fields.interval.valid }">
    <label for="interval" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.dashboard.columns.interval') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.interval" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('interval'), 'form-control-success': fields.interval && fields.interval.valid}" id="interval" name="interval" placeholder="{{ trans('admin.dashboard.columns.interval') }}">
        <div v-if="errors.has('interval')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('interval') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('interval_count'), 'has-success': fields.interval_count && fields.interval_count.valid }">
    <label for="interval_count" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.dashboard.columns.interval_count') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.interval_count" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('interval_count'), 'form-control-success': fields.interval_count && fields.interval_count.valid}" id="interval_count" name="interval_count" placeholder="{{ trans('admin.dashboard.columns.interval_count') }}">
        <div v-if="errors.has('interval_count')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('interval_count') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('current_period_end'), 'has-success': fields.current_period_end && fields.current_period_end.valid }">
    <label for="current_period_end" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.dashboard.columns.current_period_end') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.current_period_end" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('current_period_end'), 'form-control-success': fields.current_period_end && fields.current_period_end.valid}" id="current_period_end" name="current_period_end" placeholder="{{ trans('admin.dashboard.columns.current_period_end') }}">
        <div v-if="errors.has('current_period_end')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('current_period_end') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('current_period_start'), 'has-success': fields.current_period_start && fields.current_period_start.valid }">
    <label for="current_period_start" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.dashboard.columns.current_period_start') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.current_period_start" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('current_period_start'), 'form-control-success': fields.current_period_start && fields.current_period_start.valid}" id="current_period_start" name="current_period_start" placeholder="{{ trans('admin.dashboard.columns.current_period_start') }}">
        <div v-if="errors.has('current_period_start')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('current_period_start') }}</div>
    </div>
</div>


