@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.dashboard.actions.index'))

@section('body')
    <link href="/css/dashboard/index.css" rel="stylesheet">
    <link href="/css/colors.css" rel="stylesheet">
    <div class="card cardBienvenida">
        <div class="card-body">
            <div class="row">
                <div>
                    <img src="/images/dashboard/bienvenido.svg" alt="Descripción de la imagen">
                </div>
                <div class="col-12 col-md-8">
                    <h5 class="card-title">Bienvenido Mauricio</h5>
                    <p class="card-text">Con nuestro panel de control descubre una forma más fácil y efectiva de gestionar
                        tus
                        sistemas</p>
                    <p class="card-text">Aquí podrás apagar y encender tus servicios de una manera sencilla y directa, ver
                        los
                        detalles de tus sistemas, gestionarlos y consultarlos en tiempo real.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="card-deck" style="margin-bottom: 20px;">
        @foreach ($statusCountsList as $status)
            <div class="card card-{{ $status['name'] }}">
                <div class="card-body">
                    <h5 class="card-title cardContentCard">{{ $status['count'] }} <img src="/images/global/credit_card.svg"
                            alt="Descripción de la imagen" class="imgCard"></h5>
                    <p class="card-text">
                        @switch($status['name'])
                            @case('incomplete')
                                incompleto
                            @break

                            @case('incomplete_expired')
                                incompleto expirado
                            @break

                            @case('trialing')
                                en prueba
                            @break

                            @case('active')
                                activo
                            @break

                            @case('past_due')
                                vencido
                            @break

                            @case('canceled')
                                cancelado
                            @break

                            @case('unpaid')
                                no pagado
                            @break

                            @case('paused')
                                en pausa
                            @break

                            @default
                                desconocido
                        @endswitch
                    </p>
                </div>
            </div>
        @endforeach
    </div>

    <dashboard-listing :data="{{ $data->toJson() }}" :url="'{{ url('admin/dashboards') }}'" inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.dashboard.actions.index') }}
                        <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0"
                            href="{{ url('admin/dashboards/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp;
                            {{ trans('admin.dashboard.actions.create') }}</a>
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control"
                                                placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}"
                                                v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary"
                                                    @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp;
                                                    {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">

                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th class="bulk-checkbox">
                                            <input class="form-check-input" id="enabled" type="checkbox"
                                                v-model="isClickedAll" v-validate="''" data-vv-name="enabled"
                                                name="enabled_fake_element" @click="onBulkItemsClickedAllWithPagination()">
                                            <label class="form-check-label" for="enabled">
                                                #
                                            </label>
                                        </th>

                                        <th is='sortable' :column="'id'" style="text-align: center; vertical-align: middle;">{{ trans('admin.dashboard.columns.id') }}
                                        </th>
                                        <th is='sortable' :column="'subscription_id'" style="text-align: center; vertical-align: middle;">
                                            {{ trans('admin.dashboard.columns.subscription_id') }}</th>
                                        <th is='sortable' :column="'nombre'" style="text-align: center; vertical-align: middle;">
                                            {{ trans('admin.dashboard.columns.nombre') }}</th>
                                        <th is='sortable' :column="'servicio'" style="text-align: center; vertical-align: middle;">
                                            {{ trans('admin.dashboard.columns.servicio') }}</th>
                                        <th is='sortable' :column="'status'" style="text-align: center; vertical-align: middle;">
                                            {{ trans('admin.dashboard.columns.status') }}</th>
                                        <th is='sortable' :column="'currency'" style="text-align: center; vertical-align: middle;">
                                            {{ trans('admin.dashboard.columns.currency') }}</th>
                                        <th is='sortable' :column="'object'" style="text-align: center; vertical-align: middle;">
                                            {{ trans('admin.dashboard.columns.object') }}</th>
                                        <th is='sortable' :column="'interval'" style="text-align: center; vertical-align: middle;">
                                            {{ trans('admin.dashboard.columns.interval') }}</th>
                                        <th is='sortable' :column="'interval_count'" style="text-align: center; vertical-align: middle;">
                                            {{ trans('admin.dashboard.columns.interval_count') }}</th>
                                        <th is='sortable' :column="'current_period_end'" style="text-align: center; vertical-align: middle;">
                                            {{ trans('admin.dashboard.columns.current_period_end') }}</th>
                                        <th is='sortable' :column="'current_period_start'" style="text-align: center; vertical-align: middle;">
                                            {{ trans('admin.dashboard.columns.current_period_start') }}</th>

                                        <th></th>
                                    </tr>
                                    <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                        <td class="bg-bulk-info d-table-cell text-center" colspan="13">
                                            <span
                                                class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }}
                                                @{{ clickedBulkItemsCount }}. <a href="#" class="text-primary"
                                                    @click="onBulkItemsClickedAll('/admin/dashboards')"
                                                    v-if="(clickedBulkItemsCount < pagination.state.total)"> <i
                                                        class="fa"
                                                        :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i>
                                                    {{ trans('brackets/admin-ui::admin.listing.check_all_items') }}
                                                    @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                    href="#" class="text-primary"
                                                    @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>
                                            </span>

                                            <span class="pull-right pr-2">
                                                <button class="btn btn-sm btn-danger pr-3 pl-3"
                                                    @click="bulkDelete('/admin/dashboards/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                            </span>

                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id"
                                        :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        <td class="bulk-checkbox">
                                            <input class="form-check-input" :id="'enabled' + item.id" type="checkbox"
                                                v-model="bulkItems[item.id]" v-validate="''"
                                                :data-vv-name="'enabled' + item.id"
                                                :name="'enabled' + item.id + '_fake_element'"
                                                @click="onBulkItemClicked(item.id)" :disabled="bulkCheckingAllLoader">
                                            <label class="form-check-label" :for="'enabled' + item.id">
                                            </label>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">@{{ item.id }}</td>
                                        <td style="text-align: center; vertical-align: middle;">@{{ item.subscription_id }}</td>
                                        <td style="text-align: center; vertical-align: middle;">@{{ item.nombre }}</td>
                                        <td style="text-align: center; vertical-align: middle;">@{{ item.servicio }}</td>
                                        <td style="text-align: center; vertical-align: middle;"><span class="status-label">@{{ item.status }}</span></td>
                                        <td style="text-align: center; vertical-align: middle;">@{{ item.currency }}</td>
                                        <td style="text-align: center; vertical-align: middle;">@{{ item.object }}</td>
                                        <td style="text-align: center; vertical-align: middle;">@{{ item.interval }}</td>
                                        <td style="text-align: center; vertical-align: middle;">@{{ item.interval_count }}</td>
                                        <td style="text-align: center; vertical-align: middle;">@{{ item.current_period_end }}</td>
                                        <td style="text-align: center; vertical-align: middle;">@{{ item.current_period_start }}</td>

                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info"
                                                        :href="item.resource_url + '/edit'"
                                                        title="{{ trans('brackets/admin-ui::admin.btn.edit') }}"
                                                        role="button"><i class="fa fa-edit"></i></a>
                                                </div>
                                                <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                    <button type="submit" class="btn btn-sm btn-danger"
                                                        title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i
                                                            class="fa fa-trash-o"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span
                                        class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                <a class="btn btn-primary btn-spinner" href="{{ url('admin/dashboards/create') }}"
                                    role="button"><i class="fa fa-plus"></i>&nbsp;
                                    {{ trans('admin.dashboard.actions.create') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </dashboard-listing>

@endsection
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var statusCells = document.querySelectorAll('.status-label');

        statusCells.forEach(function(cell) {
            var status = cell.textContent.trim();
            console.log('Status:', status);
            switch (status) {
                case 'incomplete':
                    cell.textContent = 'incompleto';
                    cell.classList.add('status-incomplete');
                    break;
                case 'incomplete_expired':
                    cell.textContent = 'incompleto expirado';
                    cell.classList.add('status-incomplete_expired');
                    break;
                case 'trialing':
                    cell.textContent = 'en prueba';
                    cell.classList.add('status-trialing');
                    break;
                case 'active':
                    cell.textContent = 'activo';
                    cell.classList.add('status-active');
                    break;
                case 'past_due':
                    cell.textContent = 'vencido';
                    cell.classList.add('status-past_due');
                    break;
                case 'canceled':
                    cell.textContent = 'cancelado';
                    cell.classList.add('status-canceled');
                    break;
                case 'unpaid':
                    cell.textContent = 'no pagado';
                    cell.classList.add('status-unpaid');
                    break;
                case 'paused':
                    cell.textContent = 'en pausa';
                    cell.classList.add('status-paused');
                    break;
                default:
                    break;
            }
        });
    });
</script>
