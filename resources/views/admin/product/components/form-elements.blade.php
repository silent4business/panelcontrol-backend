
<div class="row form-inline" style="padding-bottom: 10px;" v-cloak>
    <div :class="{'col-xl-10 col-md-11 text-right': !isFormLocalized, 'col text-center': isFormLocalized, 'hidden': onSmallScreen }">
        <small>{{ trans('brackets/admin-ui::admin.forms.currently_editing_translation') }}<span v-if="!isFormLocalized && otherLocales.length > 1"> {{ trans('brackets/admin-ui::admin.forms.more_can_be_managed') }}</span><span v-if="!isFormLocalized"> | <a href="#" @click.prevent="showLocalization">{{ trans('brackets/admin-ui::admin.forms.manage_translations') }}</a></span></small>
        <i class="localization-error" v-if="!isFormLocalized && showLocalizedValidationError"></i>
    </div>

    <div class="col text-center" :class="{'language-mobile': onSmallScreen, 'has-error': !isFormLocalized && showLocalizedValidationError}" v-if="isFormLocalized || onSmallScreen" v-cloak>
        <small>{{ trans('brackets/admin-ui::admin.forms.choose_translation_to_edit') }}
            <select class="form-control" v-model="currentLocale">
                <option :value="defaultLocale" v-if="onSmallScreen">@{{defaultLocale.toUpperCase()}}</option>
                <option v-for="locale in otherLocales" :value="locale">@{{locale.toUpperCase()}}</option>
            </select>
            <i class="localization-error" v-if="isFormLocalized && showLocalizedValidationError"></i>
            <span>|</span>
            <a href="#" @click.prevent="hideLocalization">{{ trans('brackets/admin-ui::admin.forms.hide') }}</a>
        </small>
    </div>
</div>

<div v-if="showDatas">
<div class="row">
    @foreach($locales as $locale)
        <div class="col-md" v-show="shouldShowLangGroup('{{ $locale }}')" v-cloak>
            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('features_{{ $locale }}'), 'has-success': fields.features_{{ $locale }} && fields.features_{{ $locale }}.valid }">
                <label for="features_{{ $locale }}" class="col-md-2 col-form-label text-md-right">{{ trans('admin.product.columns.features') }}</label>
                <div class="col-md-9" :class="{'col-xl-8': !isFormLocalized }">
                    <input type="text" v-model="form.features.{{ $locale }}"  class="form-control" :class="{'form-control-danger': errors.has('features_{{ $locale }}'), 'form-control-success': fields.features_{{ $locale }} && fields.features_{{ $locale }}.valid }" id="features_{{ $locale }}" name="features_{{ $locale }}" placeholder="{{ trans('admin.product.columns.features') }}">
                    <div v-if="errors.has('features_{{ $locale }}')" class="form-control-feedback form-text" v-cloak>{{'{{'}} errors.first('features_{{ $locale }}') }}</div>
                </div>
            </div>
        </div>
    @endforeach
</div>
</div>

<div class="row">
    @foreach($locales as $locale)
        <div class="col-md" v-show="shouldShowLangGroup('{{ $locale }}')" v-cloak>
            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('images_{{ $locale }}'), 'has-success': fields.images_{{ $locale }} && fields.images_{{ $locale }}.valid }">
                <label for="images_{{ $locale }}" class="col-md-2 col-form-label text-md-right">{{ trans('admin.product.columns.images') }}</label>
                <div class="col-md-9" :class="{'col-xl-8': !isFormLocalized }">
                    <input type="text" v-model="form.images.{{ $locale }}"  class="form-control" :class="{'form-control-danger': errors.has('images_{{ $locale }}'), 'form-control-success': fields.images_{{ $locale }} && fields.images_{{ $locale }}.valid }" id="images_{{ $locale }}" name="images_{{ $locale }}" placeholder="{{ trans('admin.product.columns.images') }}">
                    <div v-if="errors.has('images_{{ $locale }}')" class="form-control-feedback form-text" v-cloak>{{'{{'}} errors.first('images_{{ $locale }}') }}</div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div v-if="showDatas">
<div class="row">
    @foreach($locales as $locale)
        <div class="col-md" v-show="shouldShowLangGroup('{{ $locale }}')" v-cloak>
            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('metadata_{{ $locale }}'), 'has-success': fields.metadata_{{ $locale }} && fields.metadata_{{ $locale }}.valid }">
                <label for="metadata_{{ $locale }}" class="col-md-2 col-form-label text-md-right">{{ trans('admin.product.columns.metadata') }}</label>
                <div class="col-md-9" :class="{'col-xl-8': !isFormLocalized }">
                    <input type="text" v-model="form.metadata.{{ $locale }}" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('metadata_{{ $locale }}'), 'form-control-success': fields.metadata_{{ $locale }} && fields.metadata_{{ $locale }}.valid }" id="metadata_{{ $locale }}" name="metadata_{{ $locale }}" placeholder="{{ trans('admin.product.columns.metadata') }}">
                    <div v-if="errors.has('metadata_{{ $locale }}')" class="form-control-feedback form-text" v-cloak>{{'{{'}} errors.first('metadata_{{ $locale }}') }}</div>
                </div>
            </div>
        </div>
    @endforeach
</div>
</div>

<div v-if="showDatas">
<div class="row">
    @foreach($locales as $locale)
        <div class="col-md" v-show="shouldShowLangGroup('{{ $locale }}')" v-cloak>
            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('package_dimensions_{{ $locale }}'), 'has-success': fields.package_dimensions_{{ $locale }} && fields.package_dimensions_{{ $locale }}.valid }">
                <label for="package_dimensions_{{ $locale }}" class="col-md-2 col-form-label text-md-right">{{ trans('admin.product.columns.package_dimensions') }}</label>
                <div class="col-md-9" :class="{'col-xl-8': !isFormLocalized }">
                    <input type="text" v-model="form.package_dimensions.{{ $locale }}" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('package_dimensions_{{ $locale }}'), 'form-control-success': fields.package_dimensions_{{ $locale }} && fields.package_dimensions_{{ $locale }}.valid }" id="package_dimensions_{{ $locale }}" name="package_dimensions_{{ $locale }}" placeholder="{{ trans('admin.product.columns.package_dimensions') }}">
                    <div v-if="errors.has('package_dimensions_{{ $locale }}')" class="form-control-feedback form-text" v-cloak>{{'{{'}} errors.first('package_dimensions_{{ $locale }}') }}</div>
                </div>
            </div>
        </div>
    @endforeach
</div>
</div>

<div v-if="showDatas">
<div class="form-check row" :class="{'has-danger': errors.has('active'), 'has-success': fields.active && fields.active.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="active" type="checkbox" v-model="form.active" v-validate="''" data-vv-name="active"  name="active_fake_element">
        <label class="form-check-label" for="active">
            {{ trans('admin.product.columns.active') }}
        </label>
        <input type="hidden" name="active" :value="form.active">
        <div v-if="errors.has('active')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('active') }}</div>
    </div>
</div>
</div>

<div v-if="showDatas">
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('created'), 'has-success': fields.created && fields.created.valid }">
    <label for="created" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.created') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.created" :config="datetimePickerConfig" v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('created'), 'form-control-success': fields.created && fields.created.valid}" id="created" name="created" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        </div>
        <div v-if="errors.has('created')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('created') }}</div>
    </div>
</div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('default_price'), 'has-success': fields.default_price && fields.default_price.valid }">
    <label for="default_price" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.default_price') }}</label>
    <div class="col-md-4">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">$</span>
            </div>
            <input type="text" v-model="formattedDefaultPrice" @input="validateInput" class="form-control" :class="{'form-control-danger': errors.has('default_price'), 'form-control-success': fields.default_price && fields.default_price.valid}" id="default_price" name="default_price" placeholder="{{ trans('admin.product.columns.default_price') }}">
            <div class="input-group-append">
                <select v-model="selectedCurrency" class="custom-select" @change="updateCurrency($event)">
                    <option value="mxn">MXN</option>
                    <option value="usd">USD</option>
                    <option value="eur">EUR</option>
                    <option value="gbp">GBP</option>
                    <!-- Add more options as needed -->
                </select>
            </div>
        </div>
        <div v-if="errors.has('default_price')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('default_price') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('description'), 'has-success': fields.description && fields.description.valid }">
    <label for="description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.description') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <textarea v-model="form.description" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('description'), 'form-control-success': fields.description && fields.description.valid}" id="description" name="description" placeholder="{{ trans('admin.product.columns.description') }}"></textarea>
        <div v-if="errors.has('description')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('description') }}</div>
    </div>
</div>

<div v-if="showDatas">
<div class="form-check row" :class="{'has-danger': errors.has('livemode'), 'has-success': fields.livemode && fields.livemode.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="livemode" type="checkbox" v-model="form.livemode" v-validate="''" data-vv-name="livemode"  name="livemode_fake_element">
        <label class="form-check-label" for="livemode">
            {{ trans('admin.product.columns.livemode') }}
        </label>
        <input type="hidden" name="livemode" :value="form.livemode">
        <div v-if="errors.has('livemode')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('livemode') }}</div>
    </div>
</div>
</div>


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name"  class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': fields.name && fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.product.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('object'), 'has-success': fields.object && fields.object.valid }">
    <label for="object" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">Period</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select v-model="form.object" class="form-control" :class="{'form-control-danger': errors.has('object'), 'form-control-success': fields.object && fields.object.valid}" id="object" name="object">
            <option value="1_month">Cada mes</option>
            <option value="3_months">Cada 3 meses</option>
            <option value="6_months">Cada 6 meses</option>
            <option value="yearly">Anual</option>
        </select>
        <div v-if="errors.has('object')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('object') }}</div>
    </div>
</div>

<div v-if="showDatas">
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('product_id'), 'has-success': fields.product_id && fields.product_id.valid }">
    <label for="product_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.product_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.product_id"  class="form-control" :class="{'form-control-danger': errors.has('product_id'), 'form-control-success': fields.product_id && fields.product_id.valid}" id="product_id" name="product_id" placeholder="{{ trans('admin.product.columns.product_id') }}">
        <div v-if="errors.has('product_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('product_id') }}</div>
    </div>
</div>
</div>
<div v-if="showDatas">
<div class="form-check row" :class="{'has-danger': errors.has('shippable'), 'has-success': fields.shippable && fields.shippable.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="shippable" type="checkbox" v-model="form.shippable" v-validate="''" data-vv-name="shippable"  name="shippable_fake_element">
        <label class="form-check-label" for="shippable">
            {{ trans('admin.product.columns.shippable') }}
        </label>
        <input type="hidden" name="shippable" :value="form.shippable">
        <div v-if="errors.has('shippable')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('shippable') }}</div>
    </div>
</div>
</div>
<div v-if="showDatas">
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('statement_descriptor'), 'has-success': fields.statement_descriptor && fields.statement_descriptor.valid }">
    <label for="statement_descriptor" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.statement_descriptor') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.statement_descriptor" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('statement_descriptor'), 'form-control-success': fields.statement_descriptor && fields.statement_descriptor.valid}" id="statement_descriptor" name="statement_descriptor" placeholder="{{ trans('admin.product.columns.statement_descriptor') }}">
        <div v-if="errors.has('statement_descriptor')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('statement_descriptor') }}</div>
    </div>
</div>
</div>
<div v-if="showDatas">
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('tax_code'), 'has-success': fields.tax_code && fields.tax_code.valid }">
    <label for="tax_code" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.tax_code') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.tax_code" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('tax_code'), 'form-control-success': fields.tax_code && fields.tax_code.valid}" id="tax_code" name="tax_code" placeholder="{{ trans('admin.product.columns.tax_code') }}">
        <div v-if="errors.has('tax_code')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('tax_code') }}</div>
    </div>
</div>
</div>
<div v-if="showDatas">
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('unit_label'), 'has-success': fields.unit_label && fields.unit_label.valid }">
    <label for="unit_label" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.unit_label') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.unit_label" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('unit_label'), 'form-control-success': fields.unit_label && fields.unit_label.valid}" id="unit_label" name="unit_label" placeholder="{{ trans('admin.product.columns.unit_label') }}">
        <div v-if="errors.has('unit_label')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('unit_label') }}</div>
    </div>
</div>
</div>
<div v-if="showDatas">
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('updated'), 'has-success': fields.updated && fields.updated.valid }">
    <label for="updated" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.updated') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.updated" :config="datetimePickerConfig" v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('updated'), 'form-control-success': fields.updated && fields.updated.valid}" id="updated" name="updated" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        </div>
        <div v-if="errors.has('updated')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('updated') }}</div>
    </div>
</div>
</div>
<div v-if="showDatas">
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('url'), 'has-success': fields.url && fields.url.valid }">
    <label for="url" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.url') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.url" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('url'), 'form-control-success': fields.url && fields.url.valid}" id="url" name="url" placeholder="{{ trans('admin.product.columns.url') }}">
        <div v-if="errors.has('url')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('url') }}</div>
    </div>
</div>
</div>


