@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.product.actions.index'))

@section('body')
<link href="/css/product/create.css" rel="stylesheet">
<link href="/css/colors.css" rel="stylesheet">

    <product-listing :data="{{ $data->toJson() }}" :url="'{{ url('admin/products') }}'" inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.product.actions.index') }}
                        <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0"
                            href="{{ url('admin/products/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp;
                            {{ trans('admin.product.actions.create') }}</a>
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control"
                                                placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}"
                                                v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary"
                                                    @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp;
                                                    {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">

                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th class="bulk-checkbox">
                                            <input class="form-check-input" id="enabled" type="checkbox"
                                                v-model="isClickedAll" v-validate="''" data-vv-name="enabled"
                                                name="enabled_fake_element" @click="onBulkItemsClickedAllWithPagination()">
                                            <label class="form-check-label" for="enabled">
                                                #
                                            </label>
                                        </th>
                                        <th is='sortable' :column="'id'">{{ trans('admin.product.columns.id') }}
                                        </th>
                                        <th is='sortable' :column="'name'">{{ trans('admin.product.columns.name') }}
                                        </th>
                                        <th is='sortable' :column="'active'">
                                            {{ trans('admin.product.columns.active') }}</th>
                                        <th is='sortable' :column="'created'">
                                            {{ trans('admin.product.columns.created') }}</th>
                                        <th is='sortable' :column="'default_price'">
                                            {{ trans('admin.product.columns.default_price') }}</th>
                                        <th is='sortable' :column="'description'">
                                            {{ trans('admin.product.columns.description') }}</th>
                                        {{-- <th is='sortable' :column="'features'">{{ trans('admin.product.columns.features') }}</th> --}}

                                        {{-- <th is='sortable' :column="'images'">{{ trans('admin.product.columns.images') }}</th> --}}
                                        {{-- <th is='sortable' :column="'livemode'">{{ trans('admin.product.columns.livemode') }}</th> --}}
                                        {{-- <th is='sortable' :column="'metadata'">{{ trans('admin.product.columns.metadata') }}</th> --}}

                                        <th is='sortable' :column="'object'">
                                            {{ trans('admin.product.columns.object') }}</th>
                                        {{-- <th is='sortable' :column="'package_dimensions'">{{ trans('admin.product.columns.package_dimensions') }}</th> --}}
                                        <th is='sortable' :column="'product_id'">
                                            {{ trans('admin.product.columns.product_id') }}</th>
                                        {{-- <th is='sortable' :column="'shippable'">{{ trans('admin.product.columns.shippable') }}</th> --}}
                                        {{-- <th is='sortable' :column="'statement_descriptor'">{{ trans('admin.product.columns.statement_descriptor') }}</th> --}}
                                        {{-- <th is='sortable' :column="'tax_code'">{{ trans('admin.product.columns.tax_code') }}</th> --}}
                                        {{-- <th is='sortable' :column="'unit_label'">{{ trans('admin.product.columns.unit_label') }}</th> --}}
                                        <th is='sortable' :column="'updated'">
                                            {{ trans('admin.product.columns.updated') }}</th>
                                        {{-- <th is='sortable' :column="'url'">{{ trans('admin.product.columns.url') }} --}}
                                        </th>

                                        <th></th>
                                    </tr>
                                    <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                        <td class="bg-bulk-info d-table-cell text-center" colspan="21">
                                            <span
                                                class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }}
                                                @{{ clickedBulkItemsCount }}. <a href="#" class="text-primary"
                                                    @click="onBulkItemsClickedAll('/admin/products')"
                                                    v-if="(clickedBulkItemsCount < pagination.state.total)"> <i
                                                        class="fa"
                                                        :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i>
                                                    {{ trans('brackets/admin-ui::admin.listing.check_all_items') }}
                                                    @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                    href="#" class="text-primary"
                                                    @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>
                                            </span>

                                            <span class="pull-right pr-2">
                                                <button class="btn btn-sm btn-danger pr-3 pl-3"
                                                    @click="bulkDelete('/admin/products/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                            </span>

                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id"
                                        :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        <td class="bulk-checkbox">
                                            <input class="form-check-input" :id="'enabled' + item.id" type="checkbox"
                                                v-model="bulkItems[item.id]" v-validate="''"
                                                :data-vv-name="'enabled' + item.id"
                                                :name="'enabled' + item.id + '_fake_element'"
                                                @click="onBulkItemClicked(item.id)" :disabled="bulkCheckingAllLoader">
                                            <label class="form-check-label" :for="'enabled' + item.id">
                                            </label>
                                        </td>
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ item.name }}</td>
                                        <td>@{{ item.active }}</td>

                                        <td>@{{ item.created | datetime }}</td>
                                        <td>@{{ item.default_price }}</td>
                                        <td>@{{ item.description }}</td>
                                        {{-- <td>@{{ item.features }}</td> --}}

                                        {{-- <td>@{{ item.images }}</td> --}}
                                        {{-- <td>@{{ item.livemode }}</td> --}}
                                        {{-- <td>@{{ item.metadata }}</td> --}}

                                        <td>@{{ item.object }}</td>
                                        {{-- <td>@{{ item.package_dimensions }}</td> --}}
                                        <td>@{{ item.product_id }}</td>
                                        {{-- <td>@{{ item.shippable }}</td> --}}
                                        {{-- <td>@{{ item.statement_descriptor }}</td> --}}
                                        {{-- <td>@{{ item.tax_code }}</td> --}}
                                        {{-- <td>@{{ item.unit_label }}</td> --}}
                                        <td>@{{ item.updated | datetime }}</td>
                                        {{-- <td>@{{ item.url }}</td> --}}

                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info"
                                                        :href="item.resource_url + '/edit'"
                                                        title="{{ trans('brackets/admin-ui::admin.btn.edit') }}"
                                                        role="button"><i class="fa fa-edit"></i></a>
                                                </div>
                                                <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                    <button type="submit" class="btn btn-sm btn-danger"
                                                        title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i
                                                            class="fa fa-trash-o"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span
                                        class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                <a class="btn btn-primary btn-spinner" href="{{ url('admin/products/create') }}"
                                    role="button"><i class="fa fa-plus"></i>&nbsp;
                                    {{ trans('admin.product.actions.create') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </product-listing>

@endsection
