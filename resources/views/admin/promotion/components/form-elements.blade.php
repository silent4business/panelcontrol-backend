<div class="row form-inline" style="padding-bottom: 10px;" v-cloak>
    <div :class="{'col-xl-10 col-md-11 text-right': !isFormLocalized, 'col text-center': isFormLocalized, 'hidden': onSmallScreen }">
        <small>{{ trans('brackets/admin-ui::admin.forms.currently_editing_translation') }}<span v-if="!isFormLocalized && otherLocales.length > 1"> {{ trans('brackets/admin-ui::admin.forms.more_can_be_managed') }}</span><span v-if="!isFormLocalized"> | <a href="#" @click.prevent="showLocalization">{{ trans('brackets/admin-ui::admin.forms.manage_translations') }}</a></span></small>
        <i class="localization-error" v-if="!isFormLocalized && showLocalizedValidationError"></i>
    </div>

    <div class="col text-center" :class="{'language-mobile': onSmallScreen, 'has-error': !isFormLocalized && showLocalizedValidationError}" v-if="isFormLocalized || onSmallScreen" v-cloak>
        <small>{{ trans('brackets/admin-ui::admin.forms.choose_translation_to_edit') }}
            <select class="form-control" v-model="currentLocale">
                <option :value="defaultLocale" v-if="onSmallScreen">@{{defaultLocale.toUpperCase()}}</option>
                <option v-for="locale in otherLocales" :value="locale">@{{locale.toUpperCase()}}</option>
            </select>
            <i class="localization-error" v-if="isFormLocalized && showLocalizedValidationError"></i>
            <span>|</span>
            <a href="#" @click.prevent="hideLocalization">{{ trans('brackets/admin-ui::admin.forms.hide') }}</a>
        </small>
    </div>
</div>

<div class="row">
    @foreach($locales as $locale)
        <div class="col-md" v-show="shouldShowLangGroup('{{ $locale }}')" v-cloak>
            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('metadata_{{ $locale }}'), 'has-success': fields.metadata_{{ $locale }} && fields.metadata_{{ $locale }}.valid }">
                <label for="metadata_{{ $locale }}" class="col-md-2 col-form-label text-md-right">{{ trans('admin.promotion.columns.metadata') }}</label>
                <div class="col-md-9" :class="{'col-xl-8': !isFormLocalized }">
                    <input type="text" v-model="form.metadata.{{ $locale }}" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('metadata_{{ $locale }}'), 'form-control-success': fields.metadata_{{ $locale }} && fields.metadata_{{ $locale }}.valid }" id="metadata_{{ $locale }}" name="metadata_{{ $locale }}" placeholder="{{ trans('admin.promotion.columns.metadata') }}">
                    <div v-if="errors.has('metadata_{{ $locale }}')" class="form-control-feedback form-text" v-cloak>{{'{{'}} errors.first('metadata_{{ $locale }}') }}</div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div class="row">
    @foreach($locales as $locale)
        <div class="col-md" v-show="shouldShowLangGroup('{{ $locale }}')" v-cloak>
            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('restrictions_{{ $locale }}'), 'has-success': fields.restrictions_{{ $locale }} && fields.restrictions_{{ $locale }}.valid }">
                <label for="restrictions_{{ $locale }}" class="col-md-2 col-form-label text-md-right">{{ trans('admin.promotion.columns.restrictions') }}</label>
                <div class="col-md-9" :class="{'col-xl-8': !isFormLocalized }">
                    <input type="text" v-model="form.restrictions.{{ $locale }}" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('restrictions_{{ $locale }}'), 'form-control-success': fields.restrictions_{{ $locale }} && fields.restrictions_{{ $locale }}.valid }" id="restrictions_{{ $locale }}" name="restrictions_{{ $locale }}" placeholder="{{ trans('admin.promotion.columns.restrictions') }}">
                    <div v-if="errors.has('restrictions_{{ $locale }}')" class="form-control-feedback form-text" v-cloak>{{'{{'}} errors.first('restrictions_{{ $locale }}') }}</div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div class="form-check row" :class="{'has-danger': errors.has('active'), 'has-success': fields.active && fields.active.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="active" type="checkbox" v-model="form.active" v-validate="''" data-vv-name="active"  name="active_fake_element">
        <label class="form-check-label" for="active">
            {{ trans('admin.promotion.columns.active') }}
        </label>
        <input type="hidden" name="active" :value="form.active">
        <div v-if="errors.has('active')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('active') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('code'), 'has-success': fields.code && fields.code.valid }">
    <label for="code" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.promotion.columns.code') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.code" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('code'), 'form-control-success': fields.code && fields.code.valid}" id="code" name="code" placeholder="{{ trans('admin.promotion.columns.code') }}">
        <div v-if="errors.has('code')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('code') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('coupon_id'), 'has-success': fields.coupon_id && fields.coupon_id.valid }">
    <label for="coupon_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.promotion.columns.coupon_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.coupon_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('coupon_id'), 'form-control-success': fields.coupon_id && fields.coupon_id.valid}" id="coupon_id" name="coupon_id" placeholder="{{ trans('admin.promotion.columns.coupon_id') }}">
        <div v-if="errors.has('coupon_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('coupon_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('created'), 'has-success': fields.created && fields.created.valid }">
    <label for="created" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.promotion.columns.created') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.created" :config="datetimePickerConfig" v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('created'), 'form-control-success': fields.created && fields.created.valid}" id="created" name="created" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        </div>
        <div v-if="errors.has('created')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('created') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('customer'), 'has-success': fields.customer && fields.customer.valid }">
    <label for="customer" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.promotion.columns.customer') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.customer" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('customer'), 'form-control-success': fields.customer && fields.customer.valid}" id="customer" name="customer" placeholder="{{ trans('admin.promotion.columns.customer') }}">
        <div v-if="errors.has('customer')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('customer') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('expires_at'), 'has-success': fields.expires_at && fields.expires_at.valid }">
    <label for="expires_at" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.promotion.columns.expires_at') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.expires_at" :config="datetimePickerConfig" v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('expires_at'), 'form-control-success': fields.expires_at && fields.expires_at.valid}" id="expires_at" name="expires_at" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        </div>
        <div v-if="errors.has('expires_at')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('expires_at') }}</div>
    </div>
</div>

<div class="form-check row" :class="{'has-danger': errors.has('livemode'), 'has-success': fields.livemode && fields.livemode.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="livemode" type="checkbox" v-model="form.livemode" v-validate="''" data-vv-name="livemode"  name="livemode_fake_element">
        <label class="form-check-label" for="livemode">
            {{ trans('admin.promotion.columns.livemode') }}
        </label>
        <input type="hidden" name="livemode" :value="form.livemode">
        <div v-if="errors.has('livemode')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('livemode') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('max_redemptions'), 'has-success': fields.max_redemptions && fields.max_redemptions.valid }">
    <label for="max_redemptions" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.promotion.columns.max_redemptions') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.max_redemptions" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('max_redemptions'), 'form-control-success': fields.max_redemptions && fields.max_redemptions.valid}" id="max_redemptions" name="max_redemptions" placeholder="{{ trans('admin.promotion.columns.max_redemptions') }}">
        <div v-if="errors.has('max_redemptions')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('max_redemptions') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('object'), 'has-success': fields.object && fields.object.valid }">
    <label for="object" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.promotion.columns.object') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.object" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('object'), 'form-control-success': fields.object && fields.object.valid}" id="object" name="object" placeholder="{{ trans('admin.promotion.columns.object') }}">
        <div v-if="errors.has('object')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('object') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('promotion_id'), 'has-success': fields.promotion_id && fields.promotion_id.valid }">
    <label for="promotion_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.promotion.columns.promotion_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.promotion_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('promotion_id'), 'form-control-success': fields.promotion_id && fields.promotion_id.valid}" id="promotion_id" name="promotion_id" placeholder="{{ trans('admin.promotion.columns.promotion_id') }}">
        <div v-if="errors.has('promotion_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('promotion_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('times_redeemed'), 'has-success': fields.times_redeemed && fields.times_redeemed.valid }">
    <label for="times_redeemed" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.promotion.columns.times_redeemed') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.times_redeemed" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('times_redeemed'), 'form-control-success': fields.times_redeemed && fields.times_redeemed.valid}" id="times_redeemed" name="times_redeemed" placeholder="{{ trans('admin.promotion.columns.times_redeemed') }}">
        <div v-if="errors.has('times_redeemed')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('times_redeemed') }}</div>
    </div>
</div>


