<?php

use App\Http\Controllers\Api\AcquisitionController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ClienteController;
use App\Http\Controllers\Api\ClienteModuloController;
use App\Http\Controllers\Api\ModulesController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\ProyectoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/auth/login', [
    AuthController::class, 'loginUser'
]);


Route::middleware(['jwt'])->group(function () {

    Route::get('/auth/logout', [
        AuthController::class, 'logout'
    ]);

    Route::resource('users', UserController::class)->except(['create', 'edit']);
    Route::resource('clients', ClienteController::class)->except(['create', 'edit']);
    Route::resource('projects', ProyectoController::class)->except(['create', 'edit']);
    Route::resource('module', ModulesController::class);
    Route::get('projects/{project_id}/{module_id}/validate', [
        ProyectoController::class,
        'validateAdquisitions'
    ]);

    Route::resource('acquisitions', AcquisitionController::class);
    Route::put('acquisitions/status/{id}', [ AcquisitionController::class, 'updateState']);
    // Route::resource('adquisicionservicio', ClienteModuloController::class);
    Route::put('/acquisitions/{id}/status', [
        AcquisitionController::class,
        'status'
    ]);
});
