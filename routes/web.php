<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return response()->json("Hola esto es un test");
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('admin-users')->name('admin-users/')->group(static function() {
            Route::get('/',                                             'AdminUsersController@index')->name('index');
            Route::get('/create',                                       'AdminUsersController@create')->name('create');
            Route::post('/',                                            'AdminUsersController@store')->name('store');
            Route::get('/{adminUser}/impersonal-login',                 'AdminUsersController@impersonalLogin')->name('impersonal-login');
            Route::get('/{adminUser}/edit',                             'AdminUsersController@edit')->name('edit');
            Route::post('/{adminUser}',                                 'AdminUsersController@update')->name('update');
            Route::delete('/{adminUser}',                               'AdminUsersController@destroy')->name('destroy');
            Route::get('/{adminUser}/resend-activation',                'AdminUsersController@resendActivationEmail')->name('resendActivationEmail');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::get('/profile',                                      'ProfileController@editProfile')->name('edit-profile');
        Route::post('/profile',                                     'ProfileController@updateProfile')->name('update-profile');
        Route::get('/password',                                     'ProfileController@editPassword')->name('edit-password');
        Route::post('/password',                                    'ProfileController@updatePassword')->name('update-password');
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('subscriptions')->name('subscriptions/')->group(static function() {
            Route::get('/',                                             'SubscriptionController@index')->name('index');
            Route::get('/create',                                       'SubscriptionController@create')->name('create');
            Route::post('/',                                            'SubscriptionController@store')->name('store');
            Route::get('/{subscription}/edit',                          'SubscriptionController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'SubscriptionController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{subscription}',                              'SubscriptionController@update')->name('update');
            Route::delete('/{subscription}',                            'SubscriptionController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('products')->name('products/')->group(static function() {
            Route::get('/',                                             'ProductController@index')->name('index');
            Route::get('/create',                                       'ProductController@create')->name('create');
            Route::post('/',                                            'ProductController@store')->name('store');
            Route::get('/{product}/edit',                               'ProductController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ProductController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{product}',                                   'ProductController@update')->name('update');
            Route::delete('/{product}',                                 'ProductController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('coupons')->name('coupons/')->group(static function() {
            Route::get('/',                                             'CouponsController@index')->name('index');
            Route::get('/create',                                       'CouponsController@create')->name('create');
            Route::post('/',                                            'CouponsController@store')->name('store');
            Route::get('/{coupon}/edit',                                'CouponsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CouponsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{coupon}',                                    'CouponsController@update')->name('update');
            Route::delete('/{coupon}',                                  'CouponsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('promotions')->name('promotions/')->group(static function() {
            Route::get('/',                                             'PromotionController@index')->name('index');
            Route::get('/create',                                       'PromotionController@create')->name('create');
            Route::post('/',                                            'PromotionController@store')->name('store');
            Route::get('/{promotion}/edit',                             'PromotionController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'PromotionController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{promotion}',                                 'PromotionController@update')->name('update');
            Route::delete('/{promotion}',                               'PromotionController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('dashboards')->name('dashboards/')->group(static function() {
            Route::get('/',                                             'DashboardController@index')->name('index');
            Route::get('/create',                                       'DashboardController@create')->name('create');
            Route::post('/',                                            'DashboardController@store')->name('store');
            Route::get('/{dashboard}/edit',                             'DashboardController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'DashboardController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{dashboard}',                                 'DashboardController@update')->name('update');
            Route::delete('/{dashboard}',                               'DashboardController@destroy')->name('destroy');
        });
    });
});